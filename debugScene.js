/// this is similar to the 'scene' function of cocos creater, 
/// can use to test a specified scene and not go through the whole game.
/// might need more implementation but i'll mainly keep it identical with prototype

var customParameter = {
    level: 'Level_01',
    // mechanical door
    mechanicalDoor: [{
        doorConfig: {
            startPosition: {
                x: 47*20,
                y: 21*20
            },
            endPosition: {
                x: 48*20,
                y: 23*20
            },
            imageName: 'mechanical_door_image_01'
        }, 
        buttonConfig: {
            startPosition: {
                x: 45*20,
                y: 26*20
            },
            endPosition: {
                x: 46*20,
                y: 27*20
            },
            imageName: 'mechanical_door_button_image_01'
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false
        }
    }, {
        doorConfig: {
            startPosition: {
                x: 47*20,
                y: 25*20
            },
            endPosition: {
                x: 48*20,
                y: 27*20
            },
            imageName: 'mechanical_door_image_02'
        }, 
        buttonConfig: {
            startPosition: {
                x: 49*20,
                y: 22*20
            },
            endPosition: {
                x: 50*20,
                y: 23*20
            },
            imageName: 'mechanical_door_button_image_02'
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: true
        }
    }],
    // gateway
    gateway: [{
        doorConfig: {
            door1: {
                startPosition: {
                    x: 29*20,
                    y: 10*20
                },
                endPosition: {
                    x: 31*20,
                    y: 13*20
                },
                imageName: 'gateway_image'
            },
            door2: {
                startPosition: {
                    x: 45*20,
                    y: 8*20
                },
                endPosition: {
                    x: 47*20,
                    y: 11*20
                },
                imageName: 'gateway_image'
            }
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false
        }
    }],
    // floating platform
    floatingPlatform: [{
        platformConfig: {
            startPosition: {
                x: 31*20,
                y: 3*20
            },
            endPosition: {
                x: 34*20,
                y: 4*20
            },
            imageName: 'floating_platform_image'
        },
        buttonConfig: {
            startPosition: {
                x: 28*20,
                y: 3*20
            },
            endPosition: {
                x: 29*20,
                y: 4*20
            },
            imageName: 'floating_platform_button_image'
        },
        moveConfig: {
            startPosition: {
                x: 32.5*20,
                y: 3.5*20
            },
            endPosition: {
                x: 42.5*20,
                y: 3.5*20
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false
        }
    }],
    // fountain
    fountain: [{
        config: {
            x: 160,
            y: 300
        }
    }],
    // blocks
    blocks: [{
        config: {
            x: 150,
            y: 200
        }
    }]
};

var debugScene = {
    init: function () {
        this.globalConfig = customParameter;
    },
    preload: function() {

        // players
        game.load.image('pixie', 'assets/pixie.png');
        game.load.spritesheet('pixie','assets/pixie_ani.png', 20, 20);

        game.load.image('pixia', 'assets/pixia.png');
        game.load.spritesheet('pixia', 'assets/pixia_ani.png', 20, 20);

        //interaction need
        game.load.image('shoot', 'assets/arrow.png');
        
        // tile assets
        game.load.image('background_tileset', 'assets/Level_01/background_tileset.png');
        game.load.image('terrain_tileset', 'assets/Level_01/terrain_tileset.png');
        game.load.tilemap('map', 'assets/Level_01/tilemap.json', null, Phaser.Tilemap.TILED_JSON);

        // mechanism image
        game.load.image('mechanical_door_image_01', 'assets/Level_01/mechanical_door_image_01.png');
        game.load.image('mechanical_door_image_02', 'assets/Level_01/mechanical_door_image_02.png');
        game.load.image('mechanical_door_button_image_01', 'assets/Level_01/mechanical_door_button_image_01.png');
        game.load.image('mechanical_door_button_image_02', 'assets/Level_01/mechanical_door_button_image_02.png');

        game.load.image('gateway_image', 'assets/Level_01/gateway_image.png');

        game.load.image('floating_platform_image', 'assets/Level_01/floating_platform_image.png');
        game.load.image('floating_platform_button_image', 'assets/Level_01/floating_platform_button_image.png');

        // other elements
        game.load.image('pixel', 'assets/pixel.png');

    },
    create: function() {

        /** basics **/

        //basic elements of game, includes bgColors, physics and rendering.
        game.time.create(true);

        // basic elements of game, includes bgColors, physics and rendering.

        game.stage.backgroundColor = '#666675';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        /** map building **/
        
        this.map = game.add.tilemap('map');
        this.map.addTilesetImage('background_tileset');
        this.map.addTilesetImage('terrain_tileset');

        this.background_layer = this.map.createLayer('BackgroundLayer');
        this.background_layer.resizeWorld();

        this.terrain_layer = this.map.createLayer('TerrainLayer');
        this.terrain_layer.resizeWorld();

        this.mechanism_layer = this.map.createLayer('MechanismLayer');
        this.mechanism_layer.resizeWorld();

        // define the collision array
        var background_tileset_collision_array = [];
        var terrain_tileset_collision_array = [3];

        this.map.setCollision(background_tileset_collision_array, true, this.background_layer);
        this.map.setCollision(terrain_tileset_collision_array, true, this.terrain_layer);

        /** player configs **/

        // player 1 configs 225, 95
        this.p1 = game.add.sprite(345, 95, 'pixie');
        this.p1.animations.add('walking', [1, 2, 3], 8, true);
        this.p1.animations.add('interact', [1, 2], 16, true);

        this.p1.anchor.setTo(0.5, 1);
        game.physics.arcade.enable(this.p1);
        //this.p1.body.drag.x = 180;
        //this.p1.body.bounce.x = 1;
        //this.p1.body.bounce.y = 0.5;
        this.p1.body.maxVelocity.x = 180;
        this.p1.body.gravity.y = 500;                       // add gravities
        this.p1.body.collideWorldBounds = true;

        this.arrow = game.add.sprite(this.p1.x, this.p1.y-20, 'shoot');
        this.arrow.anchor.setTo(0.5, 1.5);
        this.arrow.visible = false;

        // player 2 configs
        this.p2 = game.add.sprite(100, 450, 'pixia');
        this.p2.animations.add('walking', [1, 2, 3], 8, true);
        this.p2.animations.add('interact', [1, 2], 16, true);

        this.p2.anchor.setTo(0.5, 1);
        game.physics.arcade.enable(this.p2);
        //this.p2.body.drag.x = 100;
        //this.p2.body.bounce.x = 1;
        //this.p2.body.bounce.y = 0.5;
        this.p2.body.maxVelocity.x = 180;
        this.p2.body.gravity.y = 500;                       // add gravities
        this.p2.body.collideWorldBounds = true;

        /** camera object **/

        /** This object is used for manipulating the camera.
        Shall be invisible in game, will stay in the middle
        of player 1 & 2 .**/

        var cameraX = ( this.p1.x + this.p2.x ) / 2;
        var cameraY = ( this.p1.y + this.p2.y ) / 2;

        this.cameraObject = game.add.sprite(cameraX, cameraY, 'pixie');
        game.physics.arcade.enable(this.cameraObject);
        this.cameraObject.anchor.setTo(0.5, 1);

        /** Set camera object invisible, 
        note that you can set it to a none-zero value for debugging. **/

        this.cameraObject.alpha = 0;

        game.camera.follow(this.cameraObject);

        /** inputs **/

        // player controls

        // for p1
        this.p1Control = game.input.keyboard.createCursorKeys();
        // for p2
        this.p2Control = game.input.keyboard.addKeys( { 'up': Phaser.KeyCode.W,
                                                        'down': Phaser.KeyCode.S,
                                                        'left': Phaser.KeyCode.A,
                                                        'right': Phaser.KeyCode.D } );

        // interactions, press 'L' to make p2 lift p1.
        // interactMode: The situation that p2 is lifting p1.
        // the situation that p1 is thrown and haven't collide on anything collidable.
        this.interactKey = game.input.keyboard.addKey(Phaser.KeyCode.L);
        this.interactMode = false;
        this.flying = false;
        this.interactKey.onDown.add(function(){
            if(this.interactMode){
                this.interactMode =false;
                this.arrow.visible = false;
            }
            else if(game.physics.arcade.collide(this.p1, this.p2)){
                this.interactMode = true;
                this.arrow.visible = true;
            }
        }, this);

        // while interacting, press '5' to throw p1. 
        // When flying, p1 can't move with cursor key until colliding anything collidable.
        this.ejectKey = game.input.keyboard.addKey(Phaser.KeyCode.NUMPAD_5);
        this.ejectKey.onDown.add(function(){
            if(this.interactMode){
                var angle = -(this.arrow.angle-90)/180*3.1415926;
                this.p1.body.velocity.setTo(450*Math.cos(angle), -450*Math.sin(angle));
                this.flying = true;
                this.interactMode = false;
                this.arrow.visible = false;
            }
        }, this);

        /* mechanism */

        // mechanical door
        this.mechanical_door = [];
        this.globalConfig.mechanicalDoor.forEach(function(element) {
            var newMechanicalDoor = new MechanicalDoor(element.doorConfig, element.buttonConfig, element.interactionConfig);
            this.mechanical_door.push(newMechanicalDoor);
        }, this);

        // gateway
        // *** Bug : players might out of carema !!!***
        this.gateway = [];
        this.globalConfig.gateway.forEach(function(element) {
            var newGateway = new GateWay(element.doorConfig, element.interactionConfig);
            this.gateway.push(newGateway);
        }, this);

        // floating platform test
        // *** Bug : players might out of carema !!!***
        this.floating_platform = [];
        this.globalConfig.floatingPlatform.forEach(function(element) {
            var newFloatingPlatform = new FloatingPlatform(element.platformConfig, element.buttonConfig, element.moveConfig, element.interactionConfig);
            this.floating_platform.push(newFloatingPlatform);
        }, this);

        // fountain
        this.fountain = [];
        this.globalConfig.fountain.forEach(function(element) {
            var newFountain = new Fountain(element.config);
            this.fountain.push(newFountain);
        }, this);

        // block
        this.blocks = game.add.group();
        this.globalConfig.blocks.forEach(function(element) {
            var newBlock = new InteractionBlock(element.config, this.blocks);
        }, this);
    },
    update: function() {

        // collisions
        this.collisionDetect();

        // drag control

        this.blocks.forEachAlive(function (element) {
            if (element.body.onFloor()) {
                element.body.drag.x = 100;
            }
            else {
                element.body.drag.x = 10;
            }
        }, this)

        if (this.p1.body.onFloor()) {
            this.p1.body.drag.x = 1800; // 10x maxVelocity
        }
        else {
            this.p1.body.drag.x = 10;
        }

        if (this.p2.body.onFloor()) {
            this.p2.body.drag.x = 1800;
        }
        else {
            this.p2.body.drag.x = 10;
        }

        // movements
        if(!this.flying)this.movePlayer(this.p1, this.p2, this.p1Control);
        this.movePlayer(this.p2, this.p1, this.p2Control);

        // animations
        if(!this.flying)this.animationHandler(this.p1, this.p1Control);
        this.animationHandler(this.p2, this.p2Control);

        // interaction
        if(this.interactMode) {
            this.interaction();
        }

        // moves camera
        this.moveCamera();

        // mechanical
        this.mechanical_door.forEach(function(element) {
            element.update(this.p1, this.p2, this.cameraObject);
        }, this);

        // gateway
        this.gateway.forEach(function(element) {
            element.update(this.p1, this.p2, this.cameraObject);
        }, this);

        // floating platform button
        this.floating_platform.forEach(function(element) {
            element.update(this.p1, this.p2, this.cameraObject);
        }, this);

        // fountain
        this.fountain.forEach(function (element) {
            element.update(this.p1, this.p2, this.blocks);
        }, this);
    },
    // collisions
    collisionDetect: function() {

        /// setCollision if there's any tile should collide with players
        // game.physics.arcade.collide(this.p1, this.background_layer);
        // game.physics.arcade.collide(this.p2, this.background_layer);

        game.physics.arcade.collide(this.p1, this.blocks);
        game.physics.arcade.collide(this.p2, this.blocks);

        game.physics.arcade.collide(this.p1, this.terrain_layer, function(){
            if(this.flying)this.flying = false;
        }, null, this);
        
        game.physics.arcade.collide(this.p2, this.terrain_layer);
        game.physics.arcade.collide(this.blocks, this.terrain_layer);

        game.physics.arcade.collide(this.p1, this.mechanism_layer);
        game.physics.arcade.collide(this.p2, this.mechanism_layer);
        game.physics.arcade.collide(this.blocks, this.mechanism_layer);
    },
    // movement
    movePlayer: function(player, anotherPlayer, ctrl) {

        // defines current player movement speed.
        var vX = player.body.velocity.x;

        /** Distance of the players.
        Note that ,by adding the player sprite size,
        the sprites stuck on the border almost correctly.
        But we probably still need a better implementation. lol **/

        let dx = Math.abs( anotherPlayer.x - player.x ) + player.body.width;
        let dy = Math.abs( anotherPlayer.y - player.y );

        // Defines whether current player is on the left of another.
        let isOnLeft = ( player.x < anotherPlayer.x );

        if(player == this.p1 && this.interactMode){
            if (ctrl.left.isDown && this.arrow.angle-90 > -180)this.arrow.angle -= 1;
            else if (ctrl.right.isDown && this.arrow.angle-90 < 0)this.arrow.angle += 1;
        }
        // player movements: move player toward certain direction
        if (ctrl.left.isDown) {

            if(player.body.onWall())
                player.body.velocity.x = 0;
            else if(player.body.onFloor())
                player.body.velocity.x = -180;
            else 
                player.body.velocity.x =  (player.body.velocity.x > 0) ? -90 :
                                        (player.body.velocity.x === 0) ? -180 : player.body.velocity.x;
        }
        if (ctrl.right.isDown) {

            if(player.body.onWall())
                player.body.velocity.x = 0;
            else if(player.body.onFloor())
                player.body.velocity.x = 180;
            else 
                player.body.velocity.x =  (player.body.velocity.x < 0) ? 90 :
                                        (player.body.velocity.x === 0) ? 180 : player.body.velocity.x;
        }

        // jump
        if (ctrl.up.isDown) {

            if(player.body.onFloor() || player.body.touching.down)
                if(!this.interactMode)player.body.velocity.y = -225;
                else player.body.velocity.y = -275;

        }
    },
    moveCamera: function(){
        var vectorx = (this.p1.x + this.p2.x) / 2 - this.cameraObject.x;
        var vectory = (this.p1.y + this.p2.y) / 2 - this.cameraObject.y;
        this.cameraObject.body.velocity.x = (vectorx < 20 && vectorx > 1) ? 20 : (vectorx > -20 && vectorx < -1) ? -20 : (Math.abs(vectorx) < 1) ? 0 : vectorx;
        this.cameraObject.body.velocity.y = (vectory < 20 && vectory > 1) ? 20 : (vectory > -20 && vectory < -1) ? -20 : (Math.abs(vectory) < 1) ? 0 : vectory;
    },
    animationHandler: function(player, ctrl){

        if(this.interactKey.isDown) {

            // interactions

            /** This is the prototype of 'interaction',
            may need implementations for other game features.**/

            //player.animations.play('interact');

            /** Do something here, down here as example : **/
            //player.x += game.rnd.realInRange(-2,2);
            //player.y += game.rnd.realInRange(-2,2);
        }
        
        //this.interactKey.onDown.add(this.ZhaMao, this)

        if(ctrl.left.isDown || ctrl.right.isDown) {

            player.animations.play('walking');

        }
        else {

            player.animations.stop();
            player.frame = 0;

        }
    },
    interaction: function(){
        game.physics.arcade.collide(this.p1, this.p2);
        this.p1.x = this.p2.x;
        this.p1.y = this.p2.y-20;
        this.arrow.x = this.p1.x;
        this.arrow.y = this.p1.y-10;
    }
};
