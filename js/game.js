// Initialize Phaser
var game = new Phaser.Game(1800, 900, Phaser.CANVAS, 'canvas');
// Define our global variable
game.global = {
    gravity: {
        normal: 800,
        fly: 250
    },
    maxVelocity: {
        x: 180
    },
    velocity: {
        x: 250,
        y: 500,
        inAir: 90,
        projectile: 600,
        zombiex: 250,
        zombiey: 400
    },
    drag: {
        inAir: 1000,
        onFloor: 1800,
        flying: 0,
        block: 1000
    }
};
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
//game.state.add('play', playState);
game.state.add('debug', _state);
// Start the 'boot' state
game.state.start('boot');