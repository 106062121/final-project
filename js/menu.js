const level_label_description_set = [ { sprite: "red_feather",    sprite_position: {x: 450, y: 350},  text: 'Teaching Level', text_position: {x: 450, y: 275},  name: 'Teaching_Level' },
                                      { sprite: "yellow_feather", sprite_position: {x: 900, y: 350},  text: 'Level Novice',   text_position: {x: 900, y: 275},  name: 'Level_Novice'   },
                                      { sprite: "green_feather",  sprite_position: {x: 1350, y: 350}, text: 'Level XX',       text_position: {x: 1350, y: 275}, name: 'Level_XX'       },
                                      { sprite: "blue_feather",   sprite_position: {x: 675, y: 650},  text: 'Level X',        text_position: {x: 675, y: 575},  name: 'LevelX'         },
                                      { sprite: "pink_feather",   sprite_position: {x: 1125, y: 650}, text: 'Level XXX',      text_position: {x: 1125, y: 575}, name: 'Level_LIU'      } ];

var menuState = {
    preload: function() {

    },
    create: function () {
        // fx
        buttonClick = game.add.audio('buttonClick');

        // bgm
        game.sound.stopAll(); 
        this.bgm = game.add.audio('bgm04');
        this.bgm.play();

        this.startLabel = game.add.text(900, 100, 'Select Level', { font: '50px Comic Sans MS', fill: '#ffffff' });
        this.startLabel.anchor.setTo(0.5, 0.5);
        
        level_label_description_set.forEach(function(label_description) {
            var new_level_label_sprite = game.add.image(label_description.sprite_position.x, label_description.sprite_position.y, label_description.sprite);
            new_level_label_sprite.anchor.setTo(0.5, 0.5);
            new_level_label_sprite.name = label_description.name;
            
            var new_level_label_text = game.add.text(label_description.text_position.x, label_description.text_position.y, label_description.text, { font: '30px Comic Sans MS', fill: '#ffffff' });
            new_level_label_text.anchor.setTo(0.5, 0.5);
            
            this.setLabelProperties(new_level_label_sprite, new_level_label_text, { font: '30px Comic Sans MS', fill: '#ffffff' }, { font: '30px Comic Sans MS', fill: '#000000' });
        }, this);
    },
    update: function() {

    },
    start: function (Label_Sprite) {
        // play th sfx
        buttonClick.play();
        
        // Start the actual game
        var level_name = eval(Label_Sprite.name);
        game.state.start('debug', true, false, level_name);
    },
    setLabelProperties: function (Label_Sprite, Label_Text, style, alter_style) {
        Label_Sprite.inputEnabled = true;
        Label_Sprite.events.onInputOver.add(function () {
            Label_Text.setStyle(alter_style);
        }, this);
        Label_Sprite.events.onInputOut.add(function () {
            Label_Text.setStyle(style);
        }, this);
        Label_Sprite.events.onInputDown.add(this.start, this);
    }
}; 