const gateway_cooldown_time = Phaser.Timer.SECOND * 1.0; // the time that this pair of gateways can be used again if a player just used them
const gateway_working_time = Phaser.Timer.SECOND * 0.1; // the time that this pair of gateways play their working animations
const gateway_reaction_ratio = {
    x: 0.8,
    y: 1.0
}; // the reaction ration of this pair of gateways
const block_accetable_velocity_error = 5;

class GateWay {
    /*
    Parameters of the MechanicalDoor's Constructor
    doorConfig: configuration of gateway doors
    {
        door1: {
            startPosition(left-top): {
                x: startPixelX,
                y: startPixelY
            },
            endPosition(right-bottom): {
                x: endPixelX,
                y: endPixelY
            },
            imageName: imageName
        },
        door2: {
            startPosition(left-top): {
                x: startPixelX,
                y: startPixelY
            },
            endPosition(right-bottom): {
                x: endPixelX,
                y: endPixelY
            },
            imageName: imageName
        },
        animation_index_set = {
                idle: [index],
                working: [index],
                cooldown: [index]
            }
    }
    interactionConfig: configuration of interactions
    {
        p1_can_use: whether p1 can use this pair of gateways (true/false),
        p2_can_use: whether p2 can use this pair of gateways (true/false),
        blocks_can_use: whether blocks can use this pair of gateways (true/false)
    }
    */    
    constructor(doorConfig, interactionConfig) {
        this.doorConfig = {
            door1: {
                position: doorConfig.door1,
                centerPosition: {
                    x: (doorConfig.door1.startPosition.x + doorConfig.door1.endPosition.x) / 2,
                    y: (doorConfig.door1.startPosition.y + doorConfig.door1.endPosition.y) / 2
                },
                width: Math.abs(doorConfig.door1.endPosition.x - doorConfig.door1.startPosition.x),
                height: Math.abs(doorConfig.door1.endPosition.y - doorConfig.door1.startPosition.y),
                imageName: doorConfig.door1.imageName
            },
            door2: {
                position: doorConfig.door2,
                centerPosition: {
                    x: (doorConfig.door2.startPosition.x + doorConfig.door2.endPosition.x) / 2,
                    y: (doorConfig.door2.startPosition.y + doorConfig.door2.endPosition.y) / 2
                },
                width: Math.abs(doorConfig.door2.endPosition.x - doorConfig.door2.startPosition.x),
                height: Math.abs(doorConfig.door2.endPosition.y - doorConfig.door2.startPosition.y),
                imageName: doorConfig.door2.imageName
            }
        };

        this.door1 = this.createDoor(this.doorConfig.door1, doorConfig.animation_index_set);
        this.door2 = this.createDoor(this.doorConfig.door2, doorConfig.animation_index_set);

        this.door_animation_state = 'idle';
        this.last_button_state = 'idle';

        this.interactionConfig = interactionConfig;

        this.doorUsing = {
            p1: false,
            p2: false,
            blocks: false
        };
    }

    isOverlap(player_position, target_position) {
        if(player_position.x >= target_position.startPosition.x && player_position.x <= target_position.endPosition.x) {
            if(player_position.y >= target_position.startPosition.y && player_position.y <= target_position.endPosition.y) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    createDoor(doorPosition, doorAnimationIndexSet) {
        var new_door = game.add.sprite(doorPosition.centerPosition.x, doorPosition.centerPosition.y, doorPosition.imageName);
        new_door.anchor.setTo(0.5, 0.5);
        new_door.scale.setTo(doorPosition.width / new_door.width, doorPosition.height / new_door.height);
        
        game.physics.arcade.enable(new_door);
        new_door.body.immovable = true;
        
        new_door.animations.add('idle', doorAnimationIndexSet.idle, 8, true);
        new_door.animations.add('working', doorAnimationIndexSet.working, 8, true);
        new_door.animations.add('cooldown', doorAnimationIndexSet.cooldown, 8, true);

        return new_door;
    }

    gatewayTouchedListener(p1, p2, blocks, camera) {
        var new_player_position = {
            p1: {
                x: p1.x,
                y: p1.y
            },
            p2: {
                x: p2.x,
                y: p2.y
            }
        };
        
        // p1
        if(this.interactionConfig.p1_can_use) {
            // p1 vs door1
            game.physics.arcade.overlap(p1, this.door1, function() {
                var player_position = {
                    x: p1.x,
                    y: p1.y
                };
                var doorReactionPosition = {
                    startPosition: {
                        x: this.doorConfig.door1.centerPosition.x - this.doorConfig.door1.width * 0.5 * gateway_reaction_ratio.x,
                        y: this.doorConfig.door1.centerPosition.y - this.doorConfig.door1.height * 0.5 * gateway_reaction_ratio.y
                    },
                    endPosition: {
                        x: this.doorConfig.door1.centerPosition.x + this.doorConfig.door1.width * 0.5 * gateway_reaction_ratio.x,
                        y: this.doorConfig.door1.centerPosition.y + this.doorConfig.door1.height * 0.5 * gateway_reaction_ratio.y
                    }
                };

                if(this.isOverlap(player_position, doorReactionPosition)) {
                    if(Math.hypot(p1.body.velocity.x, p1.body.velocity.y) != 0 && !this.doorUsing.p1) {
                        new_player_position.p1.x = this.doorConfig.door2.centerPosition.x + ((p1.x <= this.doorConfig.door1.centerPosition.x) ? ((p1.body.velocity.x == 0) ? -1 : 1) : ((p1.body.velocity.x == 0) ? 1 : -1)) * this.doorConfig.door2.width * 0.5 * gateway_reaction_ratio.x;
                        new_player_position.p1.y = this.doorConfig.door2.centerPosition.y + (p1.y - this.doorConfig.door1.centerPosition.y);
                        this.door_animation_state = 'working';
                        this.sfxHandler();

                        this.doorUsing.p1 = true;
                        game.time.events.add(gateway_working_time, function() {
                            this.door_animation_state = 'cooldown';
                        }, this);

                        game.time.events.add(gateway_cooldown_time, function() {
                            this.doorUsing.p1 = false;
                            this.door_animation_state = 'idle';
                        }, this);
                    }
                }
            }, null, this);

            // p1 vs door2
            game.physics.arcade.overlap(p1, this.door2, function() {
                var player_position = {
                    x: p1.x,
                    y: p1.y
                };
                var doorReactionPosition = {
                    startPosition: {
                        x: this.doorConfig.door2.centerPosition.x - this.doorConfig.door2.width * 0.5 * gateway_reaction_ratio.x,
                        y: this.doorConfig.door2.centerPosition.y - this.doorConfig.door2.height * 0.5 * gateway_reaction_ratio.y
                    },
                    endPosition: {
                        x: this.doorConfig.door2.centerPosition.x + this.doorConfig.door2.width * 0.5 * gateway_reaction_ratio.x,
                        y: this.doorConfig.door2.centerPosition.y + this.doorConfig.door2.height * 0.5 * gateway_reaction_ratio.y
                    }
                };

                if(this.isOverlap(player_position, doorReactionPosition)) {
                    if(Math.hypot(p1.body.velocity.x, p1.body.velocity.y) != 0 && !this.doorUsing.p1) {
                        new_player_position.p1.x = this.doorConfig.door1.centerPosition.x + ((p1.x <= this.doorConfig.door2.centerPosition.x) ? ((p1.body.velocity.x == 0) ? -1 : 1) : ((p1.body.velocity.x == 0) ? 1 : -1)) * this.doorConfig.door1.width * 0.5 * gateway_reaction_ratio.x;
                        new_player_position.p1.y = this.doorConfig.door1.centerPosition.y + (p1.y - this.doorConfig.door2.centerPosition.y);
                        this.door_animation_state = 'working';
                        this.sfxHandler();

                        this.doorUsing.p1 = true;
                        game.time.events.add(gateway_working_time, function() {
                            this.door_animation_state = 'cooldown';
                        }, this);

                        game.time.events.add(gateway_cooldown_time, function() {
                            this.doorUsing.p1 = false;
                            this.door_animation_state = 'idle';
                        }, this);
                    }
                }
            }, null, this);
        }

        // p2
        if(this.interactionConfig.p2_can_use) {
            // p2 vs door1
            game.physics.arcade.overlap(p2, this.door1, function() {
                var player_position = {
                    x: p2.x,
                    y: p2.y
                };
                var doorReactionPosition = {
                    startPosition: {
                        x: this.doorConfig.door1.centerPosition.x - this.doorConfig.door1.width * 0.5 * gateway_reaction_ratio.x,
                        y: this.doorConfig.door1.centerPosition.y - this.doorConfig.door1.height * 0.5 * gateway_reaction_ratio.y
                    },
                    endPosition: {
                        x: this.doorConfig.door1.centerPosition.x + this.doorConfig.door1.width * 0.5 * gateway_reaction_ratio.x,
                        y: this.doorConfig.door1.centerPosition.y + this.doorConfig.door1.height * 0.5 * gateway_reaction_ratio.y
                    }
                };

                if(this.isOverlap(player_position, doorReactionPosition)) {
                    if(Math.hypot(p2.body.velocity.x, p2.body.velocity.y) != 0 && !this.doorUsing.p2) {
                        new_player_position.p2.x = this.doorConfig.door2.centerPosition.x + ((p2.x <= this.doorConfig.door1.centerPosition.x) ? ((p2.body.velocity.x == 0) ? -1 : 1) : ((p2.body.velocity.x == 0) ? 1 : -1)) * this.doorConfig.door2.width * 0.5 * gateway_reaction_ratio.x;
                        new_player_position.p2.y = this.doorConfig.door2.centerPosition.y + (p2.y - this.doorConfig.door1.centerPosition.y);
                        this.door_animation_state = 'working';
                        this.sfxHandler();
                        
                        this.doorUsing.p2 = true;
                        game.time.events.add(gateway_working_time, function() {
                            this.door_animation_state = 'cooldown';
                        }, this);
                        
                        game.time.events.add(gateway_cooldown_time, function() {
                            this.doorUsing.p2 = false;
                            this.door_animation_state = 'idle';
                        }, this);
                    }
                }
            }, null, this);

            // p2 vs door2
            game.physics.arcade.overlap(p2, this.door2, function() {
                var player_position = {
                    x: p2.x,
                    y: p2.y
                };
                var doorReactionPosition = {
                    startPosition: {
                        x: this.doorConfig.door2.centerPosition.x - this.doorConfig.door2.width * 0.5 * gateway_reaction_ratio.x,
                        y: this.doorConfig.door2.centerPosition.y - this.doorConfig.door2.height * 0.5 * gateway_reaction_ratio.y
                    },
                    endPosition: {
                        x: this.doorConfig.door2.centerPosition.x + this.doorConfig.door2.width * 0.5 * gateway_reaction_ratio.x,
                        y: this.doorConfig.door2.centerPosition.y + this.doorConfig.door2.height * 0.5 * gateway_reaction_ratio.y
                    }
                };

                if(this.isOverlap(player_position, doorReactionPosition)) {
                    if(Math.hypot(p2.body.velocity.x, p2.body.velocity.y) != 0 && !this.doorUsing.p2) {
                        new_player_position.p2.x = this.doorConfig.door1.centerPosition.x + ((p2.x <= this.doorConfig.door2.centerPosition.x) ? ((p2.body.velocity.x == 0) ? -1 : 1) : ((p2.body.velocity.x == 0) ? 1 : -1)) * this.doorConfig.door1.width * 0.5 * gateway_reaction_ratio.x;
                        new_player_position.p2.y = this.doorConfig.door1.centerPosition.y + (p2.y - this.doorConfig.door2.centerPosition.y);
                        this.door_animation_state = 'working';
                        this.sfxHandler();

                        this.doorUsing.p2 = true;
                        game.time.events.add(gateway_working_time, function() {
                            this.door_animation_state = 'cooldown';
                        }, this);

                        game.time.events.add(gateway_cooldown_time, function() {
                            this.doorUsing.p2 = false;
                            this.door_animation_state = 'idle';
                        }, this);
                    }
                }
            }, null, this);
        }
        
        var new_blocks_position = [];
        blocks.forEach(function(block) {
            var new_block_position = {
                x: block.x,
                y: block.y
            };
            new_blocks_position.push(new_block_position);
        });

        // blocks
        var block_counter = 0;
        if(this.interactionConfig.blocks_can_use) {
            blocks.forEach(function(block) {
                // block vs door1
                game.physics.arcade.overlap(block, this.door1, function() {
                    var block_position = {
                        x: block.x,
                        y: block.y
                    };
                    var doorReactionPosition = {
                        startPosition: {
                            x: this.doorConfig.door1.centerPosition.x - this.doorConfig.door1.width * 0.5 * gateway_reaction_ratio.x,
                            y: this.doorConfig.door1.centerPosition.y - this.doorConfig.door1.height * 0.5 * gateway_reaction_ratio.y
                        },
                        endPosition: {
                            x: this.doorConfig.door1.centerPosition.x + this.doorConfig.door1.width * 0.5 * gateway_reaction_ratio.x,
                            y: this.doorConfig.door1.centerPosition.y + this.doorConfig.door1.height * 0.5 * gateway_reaction_ratio.y
                        }
                    };

                    if(this.isOverlap(block_position, doorReactionPosition)) {
                        if(Math.hypot(block.body.velocity.x, block.body.velocity.y) > block_accetable_velocity_error && !this.doorUsing.blocks) {
                            new_blocks_position[block_counter].x = this.doorConfig.door2.centerPosition.x + ((block.x <= this.doorConfig.door1.centerPosition.x) ? ((Math.abs(block.body.velocity.x) < block_accetable_velocity_error) ? -1 : 1) : ((Math.abs(block.body.velocity.x) < block_accetable_velocity_error) ? 1 : -1)) * this.doorConfig.door2.width * 0.5 * gateway_reaction_ratio.x;
                            new_blocks_position[block_counter].y = this.doorConfig.door2.centerPosition.y + (block.y - this.doorConfig.door1.centerPosition.y);
                            this.door_animation_state = 'working';
                            this.sfxHandler();

                            this.doorUsing.blocks = true;
                            game.time.events.add(gateway_working_time, function() {
                                this.door_animation_state = 'cooldown';
                            }, this);

                            game.time.events.add(gateway_cooldown_time, function() {
                                this.doorUsing.blocks = false;
                                this.door_animation_state = 'idle';
                            }, this);
                        };
                    }
                }, null, this);

                // block vs door2
                game.physics.arcade.overlap(block, this.door2, function() {
                    var block_position = {
                        x: block.x,
                        y: block.y
                    };
                    var doorReactionPosition = {
                        startPosition: {
                            x: this.doorConfig.door2.centerPosition.x - this.doorConfig.door2.width * 0.5 * gateway_reaction_ratio.x,
                            y: this.doorConfig.door2.centerPosition.y - this.doorConfig.door2.height * 0.5 * gateway_reaction_ratio.y
                        },
                        endPosition: {
                            x: this.doorConfig.door2.centerPosition.x + this.doorConfig.door2.width * 0.5 * gateway_reaction_ratio.x,
                            y: this.doorConfig.door2.centerPosition.y + this.doorConfig.door2.height * 0.5 * gateway_reaction_ratio.y
                        }
                    };

                    if(this.isOverlap(block_position, doorReactionPosition)) {
                        if(Math.hypot(block.body.velocity.x, block.body.velocity.y) != 0 && !this.doorUsing.blocks) {
                            new_blocks_position[block_counter].x = this.doorConfig.door1.centerPosition.x + ((block.x <= this.doorConfig.door2.centerPosition.x) ? ((Math.abs(block.body.velocity.x) < block_accetable_velocity_error) ? -1 : 1) : ((Math.abs(block.body.velocity.x) < block_accetable_velocity_error) ? 1 : -1)) * this.doorConfig.door1.width * 0.5 * gateway_reaction_ratio.x;
                            new_blocks_position[block_counter].y = this.doorConfig.door1.centerPosition.y + (block.y - this.doorConfig.door2.centerPosition.y);
                            this.door_animation_state = 'working';
                            this.sfxHandler();

                            this.doorUsing.blocks = true;
                            game.time.events.add(gateway_working_time, function() {
                                this.door_animation_state = 'cooldown';
                            }, this);

                            game.time.events.add(gateway_cooldown_time, function() {
                                this.doorUsing.blocks = false;
                                this.door_animation_state = 'idle';
                            }, this);
                        };
                    }
                }, null, this);

                block_counter++;
            }, this);
        }

        // set player to new position
        p1.x = new_player_position.p1.x;
        p1.y = new_player_position.p1.y;
        p2.x = new_player_position.p2.x;
        p2.y = new_player_position.p2.y;

        // set blocks to new position
        var block_counter = 0;
        blocks.forEach(function(block) {
            block.x = new_blocks_position[block_counter].x;
            block.y = new_blocks_position[block_counter].y;
            block_counter++;
        }, this);

        this.last_button_state = this.door_animation_state;
    }

    gatewayAnimationHandler() {
        this.door1.animations.play(this.door_animation_state);
        this.door2.animations.play(this.door_animation_state);
    }

    sfxHandler() {
        if (this.last_button_state != this.button_animation_state) {
            _state.playFX('gateway');
        }
    }

    update(p1, p2, blocks, camera) {
        this.gatewayTouchedListener(p1, p2, blocks, camera);

        this.gatewayAnimationHandler();
    }
}