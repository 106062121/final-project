class Trap {
    /*
    Parameters of the Trap's Constructor
    trapConfig: configuration of floating trap
    {
        startPosition(left-top): {
            x: startPixelX,
            y: startPixelY
        },
        endPosition(right-bottom): {
            x: startPixelX,
            y: startPixelY
        },
        imageName: imageName
    }
    */
    constructor(trapConfig) {
        this.trapConfig = {
            position: {
                startPosition: {
                    x: trapConfig.startPosition.x,
                    y: trapConfig.startPosition.y
                },
                endPosition: {
                    x: trapConfig.endPosition.x,
                    y: trapConfig.endPosition.y
                }
            },
            centerPosition: {
                x: (trapConfig.startPosition.x + trapConfig.endPosition.x) / 2,
                y: (trapConfig.startPosition.y + trapConfig.endPosition.y) / 2
            },
            width: Math.abs(trapConfig.endPosition.x - trapConfig.startPosition.x),
            height: Math.abs(trapConfig.endPosition.y - trapConfig.startPosition.y),
            imageName: trapConfig.imageName
        };
        this.trap = this.createTrap(this.trapConfig);
    }

    createTrap(trap) {
        var new_trap = game.add.sprite(trap.centerPosition.x, trap.centerPosition.y, trap.imageName);
        new_trap.anchor.setTo(0.5, 0.5);
        new_trap.scale.setTo(trap.width / new_trap.width, trap.height / new_trap.height);
        
        game.physics.arcade.enable(new_trap);
        new_trap.body.immovable = true;
        
        return new_trap;
    }

    trapTouchedListener(p1, p2, blocks, camera) {
        var restart_game = false;
        
        // p1 vs trap
        game.physics.arcade.overlap(p1, this.trap, function() {
            if(p1.y <= this.trapConfig.position.startPosition.y && !p1.zombinize) {
                restart_game = true;
                _state.playFX('trap');
            }
        }, null, this);

        // p2 vs trap
        game.physics.arcade.overlap(p2, this.trap, function() {
            if(p2.y <= this.trapConfig.position.startPosition.y && !p2.zombinize) {
                restart_game = true;
                _state.playFX('trap');
            }
        }, null, this);

        if(restart_game) {
            _state.restartGame();
        }
    }

    update(p1, p2, blocks) {
        this.trapTouchedListener(p1, p2, blocks);

        game.physics.arcade.collide(p1, this.trap);
        game.physics.arcade.collide(p2, this.trap);
        game.physics.arcade.collide(blocks, this.trap);
    }
}