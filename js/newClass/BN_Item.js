class BN_Item {
    /*
    Parameters of BN_Item's Constructor
    x: 
    y:
    type: Define the type of the item.
    BN_Items: group reference
    */
    constructor(config, item) {
        if(config){
            if(config.type == 'Z'){ // Zombinize
                this.BN_Item = game.add.sprite(config.x, config.y, 'Posion', 0, item);
                this.BN_Item.scale.setTo(2,2);
            }
            else if(config.type == 'C'){ // Cure
                this.BN_Item = game.add.sprite(config.x, config.y, 'Cure', 0, item);
                this.BN_Item.scale.setTo(2,2);
            }
            this.BN_Item.anchor.setTo(0.5, 1);
            game.physics.arcade.enable(this.BN_Item);
            this.BN_Item.body.immovable = true;
            this.BN_Item.type = config.type;
        }
        
    }
}