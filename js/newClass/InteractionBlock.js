class InteractionBlock {
    /*
    Parameters of Block's Constructor
    x:
    y:
    blocks: group reference
    */
    constructor(config, blocks) {
        this.block = (config.blockable) ? game.add.sprite(config.x, config.y, 'Stone', 0, blocks) : game.add.sprite(config.x, config.y, 'WoodBox', 0, blocks);
        this.block.anchor.setTo(0.5, 1);
        game.physics.arcade.enable(this.block);
        this.block.body.drag.x = 100;
        this.block.body.bounce.x = 1;
        //this.block.body.bounce.y = 0.5;
        this.block.body.gravity.y = 250;
        this.block.body.collideWorldBounds = true;

        this.block.blockable = config.blockable;
    }
}