const refresh_frequency = 60; // Phaser update frequency
const door_stop_velocity_percentage = 0.02;  // DO NOT set it less than 0.02

class MechanicalDoor {
    /*
    Parameters of the MechanicalDoor's Constructor
    doorConfig: configuration of mechanical doors
    {
        startPosition(left-top): {
            x: startPixelX,
            y: startPixelY
        },
        endPosition(right-bottom): {
            x: endPixelX,
            y: endPixelY
        },
        moveVelocity: moveVelocity
        imageName: imageName
    }
    buttonConfig: configuration of buttons
    {
        startPosition(left-top): {
            x: startPixelX,
            y: startPixelY
        },
        endPosition(right-bottom): {
            x: endPixelX,
            y: endPixelY
        },
        imageName: imageName
    }
    interactionConfig: configuration of interactions
    {
        p1_can_use: whether p1 can trigger this mechanical door by button (true/false),
        p2_can_use: whether p2 can trigger this mechanical door by button (true/false),
        blocks_can_use: whether blocks can trigger this mechanical door by button (true/false)
    }
    */
    constructor(doorConfig, buttonConfig, interactionConfig) {
        this.doorConfig = {
            position: {
                startPosition: {
                    x: doorConfig.startPosition.x,
                    y: doorConfig.startPosition.y
                },
                endPosition: {
                    x: doorConfig.endPosition.x,
                    y: doorConfig.endPosition.y
                }
            },
            centerPosition: {
                x: (doorConfig.startPosition.x + doorConfig.endPosition.x) / 2,
                y: (doorConfig.startPosition.y + doorConfig.endPosition.y) / 2
            },
            width: Math.abs(doorConfig.endPosition.x - doorConfig.startPosition.x),
            height: Math.abs(doorConfig.endPosition.y - doorConfig.startPosition.y),
            imageName: doorConfig.imageName
        };
        this.door = this.createDoor(this.doorConfig);
        this.doorMoveVelocity = doorConfig.moveVelocity;

        this.buttonConfig = {
            position: {
                startPosition: {
                    x: buttonConfig.startPosition.x,
                    y: buttonConfig.startPosition.y
                },
                endPosition: {
                    x: buttonConfig.endPosition.x,
                    y: buttonConfig.endPosition.y
                }
            },
            centerPosition: {
                x: (buttonConfig.startPosition.x + buttonConfig.endPosition.x) / 2,
                y: (buttonConfig.startPosition.y + buttonConfig.endPosition.y) / 2
            },
            width: Math.abs(buttonConfig.endPosition.x - buttonConfig.startPosition.x),
            height: Math.abs(buttonConfig.endPosition.y - buttonConfig.startPosition.y),
            imageName: buttonConfig.imageName
        };
        this.button = this.createButton(this.buttonConfig);

        this.door_animation_state = 'idle';
        this.button_animation_state = 'button_up';
        this.last_button_state = 'button_up';

        this.interactionConfig = interactionConfig;

        this.buttonDown = true;

        this.doorMoving = false;
    }

    createDoor(door) {
        var new_door = game.add.sprite(door.centerPosition.x, door.centerPosition.y, door.imageName);
        this.door_zoom_rate = {
            x: door.width / new_door.width,
            y: door.height / new_door.height
        };
        new_door.anchor.setTo(0.5, 0.5);
        new_door.scale.setTo(door.width / new_door.width, door.height / new_door.height);
        
        game.physics.arcade.enable(new_door);
        new_door.body.immovable = true;

        new_door.animations.add('idle', [0], 8, true);

        return new_door;
    }

    createButton(button) {
        var new_button = game.add.sprite(button.centerPosition.x, button.centerPosition.y, button.imageName);
        new_button.anchor.setTo(0.5, 0.5);
        new_button.scale.setTo(button.width / new_button.width, button.height / new_button.height);
        
        game.physics.arcade.enable(new_button);
        new_button.body.immovable = true;

        new_button.animations.add('button_up', [0], 8, true);
        new_button.animations.add('button_down', [1], 8, true);
        /*
        new_button.animations.add('button_up', buttonAnimationIndexSet.button_up, 8, true);
        new_button.animations.add('button_down', buttonAnimationIndexSet.button_down, 8, true);
        */
        return new_button;
    }
    
    buttonTouchedListener(p1, p2, blocks) {
        this.buttonDown = false;
        this.button_animation_state = 'button_up';

        if(this.interactionConfig.p1_can_use) {
            game.physics.arcade.overlap(p1, this.button, function() {
                this.buttonDown = true;
                this.button_animation_state = 'button_down';
            }, null, this);
        }

        if(this.interactionConfig.p2_can_use) {
            game.physics.arcade.overlap(p2, this.button, function() {
                this.buttonDown = true;
                this.button_animation_state = 'button_down';
            }, null, this);
        }

        if(this.interactionConfig.blocks_can_use) {
            blocks.forEach(function(block) {
                game.physics.arcade.overlap(block, this.button, function() {
                    this.buttonDown = true;
                    this.button_animation_state = 'button_down';
                }, null, this);
            }, this);
        }
    }

    doorMove(p1, p2, blocks) {
        var target_position = {
            x: this.doorConfig.centerPosition.x,
            y: this.doorConfig.centerPosition.y + ((this.buttonDown) ? 1 : 0) * this.doorConfig.height * 0.5
        };

        var displacement = {
            x: target_position.x - this.door.x,
            y: target_position.y - this.door.y
        };
        var distance = Math.hypot(displacement.x, displacement.y);
        
        if(distance >= this.doorMoveVelocity * door_stop_velocity_percentage) {
            var unit_vector = {
                x: displacement.x / distance,
                y: displacement.y / distance
            };

            this.door.x += this.doorMoveVelocity * (1/refresh_frequency) * unit_vector.x;
            this.door.y += this.doorMoveVelocity * (1/refresh_frequency) * unit_vector.y;

            this.doorMoving = true;
        }
        else {
            this.door.body.velocity.x = 0;
            this.door.body.velocity.y = 0;

            this.door.x = target_position.x;
            this.door.y = target_position.y;

            this.doorMoving = false;
        }

        //this.door.scale.setTo(this.door_zoom_rate.x * (1 - (Math.abs(this.door.x - this.doorConfig.centerPosition.x) * 2 / this.doorConfig.width)), this.door_zoom_rate.y * (1 - (Math.abs(this.door.y - this.doorConfig.centerPosition.y) * 2 / this.doorConfig.height)));
        // crop
        var cropRect = new Phaser.Rectangle(0, 0, this.doorConfig.width, this.doorConfig.height - (this.door.y - this.doorConfig.centerPosition.y) * 2);
        this.door.crop(cropRect)
        this.door.updateCrop();
        /*
        // bounce away objects when door move
        // p1
        game.physics.arcade.overlap(p1, this.door, function() {
            if(p1.x <= this.doorConfig.centerPosition.x) {
                p1.x = this.doorConfig.position.startPosition.x - Math.abs(p1.width * 0.5);
            }
            else {
                p1.x = this.doorConfig.position.endPosition.x + Math.abs(p1.width * 0.5);
            }
        }, null, this);
        
        // p2
        game.physics.arcade.overlap(p2, this.door, function() {
            if(p2.x <= this.doorConfig.centerPosition.x) {
                p2.x = this.doorConfig.position.startPosition.x - Math.abs(p2.width * 0.5);
            }
            else {
                p2.x = this.doorConfig.position.endPosition.x + Math.abs(p2.width * 0.5);
            }
        }, null, this);

        // blocks
        blocks.forEach(function(block) {
            game.physics.arcade.overlap(block, this.door, function() {
                if(block.x <= this.doorConfig.centerPosition.x) {
                    block.x = this.doorConfig.position.startPosition.x - block.width * 0.5;
                }
                else {
                    block.x = this.doorConfig.position.endPosition.x + block.width * 0.5;
                }
            }, null, this);
        }, this);
        */
    }

    doorAnimationHandler() {
        this.door.animations.play(this.door_animation_state);
    }

    buttonAnimationHandler() {
        this.button.animations.play(this.button_animation_state);
    }

    sfxHandler() {
        if(this.last_buttonDown == false && this.buttonDown == true) {
            _state.playFX('floatingPlatformTrigger');
        }
    }

    update(p1, p2, blocks, camera) {
        this.buttonTouchedListener(p1, p2, blocks);
        this.doorMove(p1, p2, blocks);

        this.doorAnimationHandler();
        this.buttonAnimationHandler();

        game.physics.arcade.collide(p1, this.door);
        game.physics.arcade.collide(p2, this.door);
        game.physics.arcade.collide(blocks, this.door);

        this.sfxHandler();
        this.last_buttonDown = this.buttonDown;
        this.last_doorMoving = this.doorMoving;
    }
}