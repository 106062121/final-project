class Fountain {
    /*
    Parameters of Fountain's Constructor
    x:
    y:
    screw it, might custom the other parameters
    */
    constructor(config) {
        this.config = config;
        this.water = game.add.group();

        /*this.emitter1 = game.add.emitter(config.x, config.y, 15);
        this.emitter1.makeParticles('pixel', 0, undefined, true);
        this.emitter1.setYSpeed(-100, -200);
        this.emitter1.setXSpeed(-50, 50);
        
        this.emitter2 = game.add.emitter(config.x, config.y, 15);
        this.emitter2.makeParticles('pixel', 0, undefined, true);
        this.emitter2.setYSpeed(-100, -200);
        this.emitter2.setXSpeed(-50, 50);*/

        this.update();
    }

    update(p1, p2, blocks) {
        // default emitter didn't work as expected so i customed my emitter using sprite builders
        this.createEmitter(2);

        // coillison detect
        this.water.forEachAlive(function (water) {
            game.physics.arcade.overlap(water, p1, this.collideHandler, null, this);
            game.physics.arcade.overlap(water, p2, this.collideHandler, null, this);
            game.physics.arcade.overlap(water, blocks, this.collideHandler, null, this);
            if (water.y > this.config.y) {
                water.kill();
            }
        }, this);

        /*this.emitter1.start(true, 500, null, 5);
        setTimeout(() => {
            this.emitter2.start(true, 500, null, 5);
          }, 250);*/
    }

    createEmitter(numberOfEmitters) {
        for (let i = 0; i < numberOfEmitters; i++) {
            var water = this.water.getFirstDead(true, this.config.x, this.config.y, 'water');
            water.anchor.setTo(0.5, 1);
            game.physics.arcade.enable(water);
            water.body.velocity.x = (Math.random() - 0.5) * 50;
            water.body.velocity.y = (Math.random() - 0.5) * 50 - this.config.height;
            water.body.gravity.y = 500;
        }
    }

    collideHandler(sprite1, sprite2) {
        if (!sprite2.blockable) {
            sprite2.body.velocity.y = -this.config.velocity || -200;
        }
        sprite1.kill();
    }
}