const button_stop_velocity_percentage = 0.02; // DO NOT set it less than 0.02

class FloatingPlatform {
    /*
    Parameters of the MechanicalDoor's Constructor
    platformConfig: configuration of floating platform
    {
        startPosition(left-top): {
            x: startPixelX,
            y: startPixelY
        },
        endPosition(right-bottom): {
            x: startPixelX,
            y: startPixelY
        },
        imageName: imageName
    }
    buttonConfig: configuration of buttons
    {
        startPosition(left-top): {
            x: startPixelX,
            y: startPixelY
        },
        endPosition(right-bottom): {
            x: startPixelX,
            y: startPixelY
        },
        imageName: imageName
    }
    moveConfig: configuration of moving floating platforms
    {
        startPosition: {
            x: startPixelX,
            y: startPixelY
        },
        endPosition: {
            x: startPixelX,
            y: startPixelY
        },
        velocity: velocity
    }
    interactionConfig: configuration of interactions
    {
        p1_can_use: whether p1 can trigger this floating platform by button (true/false),
        p2_can_use: whether p2 can trigger this floating platform by button (true/false),
        blocks_can_use: whether blocks can trigger this floating platform by button (true/false)
    }
    */
    constructor(platformConfig, buttonConfig, moveConfig, interactionConfig) {
        this.platformConfig = {
            position: {
                startPosition: {
                    x: platformConfig.startPosition.x,
                    y: platformConfig.startPosition.y
                },
                endPosition: {
                    x: platformConfig.endPosition.x,
                    y: platformConfig.endPosition.y
                }
            },
            centerPosition: {
                x: (platformConfig.startPosition.x + platformConfig.endPosition.x) / 2,
                y: (platformConfig.startPosition.y + platformConfig.endPosition.y) / 2
            },
            width: Math.abs(platformConfig.endPosition.x - platformConfig.startPosition.x),
            height: Math.abs(platformConfig.endPosition.y - platformConfig.startPosition.y),
            imageName: platformConfig.imageName
        };
        this.platform = this.createPlatform(this.platformConfig, platformConfig.animation_index_set);

        this.buttonConfig = {
            position: {
                startPosition: {
                    x: buttonConfig.startPosition.x,
                    y: buttonConfig.startPosition.y
                },
                endPosition: {
                    x: buttonConfig.endPosition.x,
                    y: buttonConfig.endPosition.y
                }
            },
            centerPosition: {
                x: (buttonConfig.startPosition.x + buttonConfig.endPosition.x) / 2,
                y: (buttonConfig.startPosition.y + buttonConfig.endPosition.y) / 2
            },
            width: Math.abs(buttonConfig.endPosition.x - buttonConfig.startPosition.x),
            height: Math.abs(buttonConfig.endPosition.y - buttonConfig.startPosition.y),
            imageName: buttonConfig.imageName
        };
        this.button = this.createButton(this.buttonConfig, buttonConfig.animation_index_set);

        this.platform_animation_state = 'idle';
        this.button_animation_state = 'button_up';
        this.last_button_state = 'button_up';

        this.moveConfig = moveConfig;

        this.interactionConfig = interactionConfig;

        this.buttonDown = false;

        this.platformMoving = false;
    }

    createPlatform(platform_position, platformAnimationIndexSet) {
        var new_platform = game.add.sprite(platform_position.centerPosition.x, platform_position.centerPosition.y, platform_position.imageName);
        new_platform.anchor.setTo(0.5, 0.5);
        new_platform.scale.setTo(platform_position.width / new_platform.width, platform_position.height / new_platform.height);
        
        game.physics.arcade.enable(new_platform);
        new_platform.body.immovable = true;

        new_platform.animations.add('idle', platformAnimationIndexSet.idle, 8, true);
        new_platform.animations.add('working', platformAnimationIndexSet.working, 8, true);

        return new_platform;
    }

    createButton(button_position, buttonAnimationIndexSet) {
        var new_button = game.add.sprite(button_position.centerPosition.x, button_position.centerPosition.y, button_position.imageName);
        new_button.anchor.setTo(0.5, 0.5);
        new_button.scale.setTo(button_position.width / new_button.width, button_position.height / new_button.height);
        
        game.physics.arcade.enable(new_button);
        new_button.body.immovable = true;

        new_button.animations.add('button_up', buttonAnimationIndexSet.button_up, 8, true);
        new_button.animations.add('button_down', buttonAnimationIndexSet.button_down, 8, true);

        return new_button;
    }

    buttonTouchedListener(p1, p2, blocks) {
        this.buttonDown = false;
        this.button_animation_state = 'button_up';

        if(this.interactionConfig.p1_can_use) {
            game.physics.arcade.overlap(p1, this.button, function() {
                this.buttonDown = true;
                this.button_animation_state = 'button_down';
            }, null, this);
        }

        if(this.interactionConfig.p2_can_use) {
            game.physics.arcade.overlap(p2, this.button, function() {
                this.buttonDown = true;
                this.button_animation_state = 'button_down';
            }, null, this);
        }

        if(this.interactionConfig.blocks_can_use) {
            blocks.forEach(function(block) {
                game.physics.arcade.overlap(block, this.button, function() {
                    this.buttonDown = true;
                    this.button_animation_state = 'button_down';
                }, null, this);
            }, this);
        }

        this.last_button_state = this.button_animation_state;
    }

    platformMove() {
        var target_position = (this.buttonDown) ? this.moveConfig.endPosition : this.moveConfig.startPosition;
        var displacement = {
            x: target_position.x - this.platform.x,
            y: target_position.y - this.platform.y
        };
        var distance = Math.hypot(displacement.x, displacement.y);

        if(distance >= this.moveConfig.velocity * button_stop_velocity_percentage) {
            var unit_vector = {
                x: displacement.x / distance,
                y: displacement.y / distance
            };

            this.platform.body.velocity.x = this.moveConfig.velocity * unit_vector.x;
            this.platform.body.velocity.y = this.moveConfig.velocity * unit_vector.y;

            this.platform_animation_state = 'working';

            this.platformMoving = true;
        }
        else {
            this.platform.body.velocity.x = 0;
            this.platform.body.velocity.y = 0;

            this.platform.x = target_position.x;
            this.platform.y = target_position.y;

            this.platform_animation_state = 'idle';

            this.platformMoving = false;
        }
    }

    platformAnimationHandler() {
        this.platform.animations.play(this.platform_animation_state);
    }

    buttonAnimationHandler() {
        this.button.animations.play(this.button_animation_state);
    }

    sfxHandler() {
        if(this.last_buttonDown == false && this.buttonDown == true) {
            _state.playFX('mechanicalDoorTrigger');
        }
    }

    update(p1, p2, blocks, carema) {
        this.buttonTouchedListener(p1, p2, blocks);
        this.platformMove();

        this.platformAnimationHandler();
        this.buttonAnimationHandler();

        if (game.physics.arcade.collide(p1, this.platform)) {
            p1.onboard = true;
        }
        if (game.physics.arcade.collide(p2, this.platform)) {
            p2.onboard = true;
        }
        game.physics.arcade.collide(blocks, this.platform);

        this.sfxHandler();
        this.last_buttonDown = this.buttonDown;
        this.last_platformMoving = this.platformMoving;
    }
}