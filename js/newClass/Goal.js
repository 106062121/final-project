class Goal {
    /*
    Parameters of Goal's Constructor
    x: ,
    y: ,
    imageName: 
    type: Define the type of the item.
    */
    constructor(config) {
            this.Goal = game.add.sprite(config.x, config.y, config.imageName);
            this.Goal.anchor.setTo(0.5, 0.5);
            game.physics.arcade.enable(this.Goal);
            this.Goal.body.immovable = true;

            return this.Goal;
        }
}