var Level_LIU = {
    level: 'Level_LIU',
    bgm: '01',
    playerpos: {
        
        p1: {
            x: 4*tilesize,
            y: 25*tilesize
        },
        p2: {
            x: 4*tilesize,
            y: 16*tilesize
        }
        
        /*
        p1: {
            x: 44.5*tilesize,
            y: 25*tilesize
        },
        p2: {
            x: 42.5*tilesize,
            y: 25*tilesize
        }
        */
        /*
        p1: {
            x: 68.5*tilesize,
            y: 25*tilesize
        },
        p2: {
            x: 66.5*tilesize,
            y: 25*tilesize
        }
        */
        /*
        p1: {
            x: 91.5*tilesize,
            y: 25*tilesize
        },
        p2: {
            x: 89.5*tilesize,
            y: 25*tilesize
        }
        */
        /*
        p1: {
            x: 73*tilesize,
            y: 13*tilesize
        },
        p2: {
            x: 75*tilesize,
            y: 13*tilesize
        }
        */
        /*
        p1: {
            x: 34*tilesize,
            y: 8*tilesize
        },
        p2: {
            x: 36*tilesize,
            y: 8*tilesize
        }
        */
        /*
        p1: {
            x: 88*tilesize,
            y: 12*tilesize
        },
        p2: {
            x: 90*tilesize,
            y: 12*tilesize
        }
        */
        /*
        p1: {
            x: 99*tilesize,
            y: 1*tilesize
        },
        p2: {
            x: 99*tilesize,
            y: 1*tilesize
        }
        */
    },
    Story: [
        'Blue：最後一根羽毛收集到之後，藥品庫就能開啟了，\nRed：到時候大家就能救得了？',
        'Blue：沒錯，就像你服下解藥一樣，你開竅囉~\nRed：一起加油吧！！'
    ],
    // mechanical door
    mechanicalDoor: [{
        doorConfig: {
            startPosition: {
                x: 36*tilesize,
                y: 25*tilesize
            },
            endPosition: {
                x: 37*tilesize,
                y: 27*tilesize
            },
            moveVelocity: 50,
            imageName: 'blue_mechanical_door_1_2'
        }, 
        buttonConfig: {
            startPosition: {
                x: 34*tilesize,
                y: 12.5*tilesize
            },
            endPosition: {
                x: 35*tilesize,
                y: 13*tilesize
            },
            imageName: 'blue_mechanical_door_button'
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }, {
        doorConfig: {
            startPosition: {
                x: 78*tilesize,
                y: 22*tilesize
            },
            endPosition: {
                x: 80*tilesize,
                y: 27*tilesize
            },
            moveVelocity: 50,
            imageName: 'white_mechanical_door_1_4'
        }, 
        buttonConfig: {
            startPosition: {
                x: 74.5*tilesize,
                y: 21.5*tilesize
            },
            endPosition: {
                x: 76.5*tilesize,
                y: 22*tilesize
            },
            imageName: 'white_mechanical_door_button'
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: false,
            blocks_can_use: true
        }
    }],
    // gateway
    gateway: [{
        doorConfig: {
            door1: {
                startPosition: {
                    x: 22*tilesize,
                    y: 21*tilesize
                },
                endPosition: {
                    x: 26*tilesize,
                    y: 27*tilesize
                },
                imageName: 'white_gateway',
            },
            door2: {
                startPosition: {
                    x: 30*tilesize,
                    y: 7*tilesize
                },
                endPosition: {
                    x: 34*tilesize,
                    y: 13*tilesize
                },
                imageName: 'white_gateway'
            },
            animation_index_set: {
                idle: [0, 1, 2],
                working: [0, 1, 2],
                cooldown: [0, 1, 2]
            }
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: false,
            blocks_can_use: true
        }
    }, {
        doorConfig: {
            door1: {
                startPosition: {
                    x: 58*tilesize,
                    y: 21*tilesize
                },
                endPosition: {
                    x: 62*tilesize,
                    y: 27*tilesize
                },
                imageName: 'red_gateway',
            },
            door2: {
                startPosition: {
                    x: 98*tilesize,
                    y: 21*tilesize
                },
                endPosition: {
                    x: 102*tilesize,
                    y: 27*tilesize
                },
                imageName: 'red_gateway'
            },
            animation_index_set: {
                idle: [0, 1, 2],
                working: [0, 1, 2],
                cooldown: [0, 1, 2]
            }
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: true,
            blocks_can_use: true
        }
    }],
    // floating platform
    floatingPlatform: [{
        platformConfig: {
            startPosition: {
                x: 18*tilesize,
                y: 19*tilesize
            },
            endPosition: {
                x: 22*tilesize,
                y: 21*tilesize
            },
            imageName: 'purple_floating_platform_2_1', /* should be 3*1 */
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 1*tilesize,
                y: 17.5*tilesize
            },
            endPosition: {
                x: 2*tilesize,
                y: 18*tilesize
            },
            imageName: 'purple_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 20*tilesize,
                y: 20*tilesize
            },
            endPosition: {
                x: 28*tilesize,
                y: 20*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: true,
            blocks_can_use: true
        }
    }, {
        platformConfig: {
            startPosition: {
                x: 52*tilesize,
                y: 23*tilesize
            },
            endPosition: {
                x: 54*tilesize,
                y: 25*tilesize
            },
            imageName: 'purple_floating_platform_2_1', /* should be 2*1 */
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 52*tilesize,
                y: 26.5*tilesize
            },
            endPosition: {
                x: 54*tilesize,
                y: 27*tilesize
            },
            imageName: 'purple_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 53*tilesize,
                y: 24*tilesize
            },
            endPosition: {
                x: 56*tilesize,
                y: 24*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: true,
            blocks_can_use: true
        }
    }, {
        platformConfig: {
            startPosition: {
                x: 44*tilesize,
                y: 25*tilesize
            },
            endPosition: {
                x: 48*tilesize,
                y: 27*tilesize
            },
            imageName: 'purple_floating_platform_2_1', /* should be 3*1 */
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 55*tilesize,
                y: 16.5*tilesize
            },
            endPosition: {
                x: 57*tilesize,
                y: 17*tilesize
            },
            imageName: 'purple_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 46*tilesize,
                y: 26*tilesize
            },
            endPosition: {
                x: 46*tilesize,
                y: 18*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: true,
            blocks_can_use: true
        }
    }, {
        platformConfig: {
            startPosition: {
                x: 73*tilesize,
                y: 22*tilesize
            },
            endPosition: {
                x: 77*tilesize,
                y: 24*tilesize
            },
            imageName: 'purple_floating_platform_2_1', /* should be 4*1 */
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 62.5*tilesize,
                y: 26.5*tilesize
            },
            endPosition: {
                x: 64.5*tilesize,
                y: 27*tilesize
            },
            imageName: 'purple_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 75*tilesize,
                y: 23*tilesize
            },
            endPosition: {
                x: 82*tilesize,
                y: 18.5*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: true,
            blocks_can_use: true
        }
    }, {
        platformConfig: {
            startPosition: {
                x: 90*tilesize,
                y: 27*tilesize
            },
            endPosition: {
                x: 98*tilesize,
                y: 29*tilesize
            },
            imageName: 'purple_floating_platform_4_1', /* should be 4*1 */
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 90*tilesize,
                y: 14.5*tilesize
            },
            endPosition: {
                x: 92*tilesize,
                y: 15*tilesize
            },
            imageName: 'purple_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 94*tilesize,
                y: 28*tilesize
            },
            endPosition: {
                x: 94*tilesize,
                y: 18*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: true,
            blocks_can_use: true
        }
    }],
    // trap
    trap: [{
        trapConfig: {
            startPosition: {
                x: 18*tilesize,
                y: 26*tilesize
            },
            endPosition: {
                x: 20*tilesize,
                y: 27*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 20*tilesize,
                y: 26*tilesize
            },
            endPosition: {
                x: 22*tilesize,
                y: 27*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 22*tilesize,
                y: 26*tilesize
            },
            endPosition: {
                x: 24*tilesize,
                y: 27*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 24*tilesize,
                y: 26*tilesize
            },
            endPosition: {
                x: 26*tilesize,
                y: 27*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 26*tilesize,
                y: 26*tilesize
            },
            endPosition: {
                x: 28*tilesize,
                y: 27*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 28*tilesize,
                y: 26*tilesize
            },
            endPosition: {
                x: 30*tilesize,
                y: 27*tilesize
            },
            imageName: 'trap_image'
        }
    }],
    // fountain
    fountain: [],
    // blocks
    blocks: [{
        config: {
            x: 22*tilesize,
            y: 26*tilesize,
            blockable: false
        }
    }, {
        config: {
            x: 53*tilesize,
            y: 23*tilesize,
            blockable: false
        }
    }, {
        config: {
            x: 67.5*tilesize,
            y: 18.5*tilesize,
            blockable: false
        }
    }],
    BN_Item: [{
        config: {
            x: 19*tilesize,
            y: 23.5*tilesize,
            type: 'Z'
        }
    }, {
        config: {
            x: 36*tilesize,
            y: 22.5*tilesize,
            type: 'C'
        }
    }],
    Goal: {
        x: 99*tilesize,
        y: 10*tilesize,
        imageName: "pink_feather_160"
    }, 
    Torch: [{
        x: 4*tilesize, 
        y: 15*tilesize
    }, {
        x: 15*tilesize, 
        y: 25*tilesize
    }, {
        x: 20*tilesize, 
        y: 4*tilesize
    }, {
        x: 32*tilesize, 
        y: 20*tilesize
    }, {
        x: 40*tilesize, 
        y: 4*tilesize
    }, {
        x: 50*tilesize, 
        y: 18*tilesize
    }, {
        x: 60*tilesize, 
        y: 4*tilesize
    }, {
        x: 71*tilesize, 
        y: 19*tilesize
    }, {
        x: 90*tilesize, 
        y: 4*tilesize
    }, {
        x: 95*tilesize, 
        y: 18*tilesize
    }]
};