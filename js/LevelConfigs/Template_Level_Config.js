var Template = {
    level: 'Template_Level',
    playerpos: {
        /*
        p1: {
            x: player1_pos_x,
            y: player1_pos_y
        },
        p2: {
            x: player2_pos_x
            y: player2_pos_y
        }
        */
    },
    Story: [],
    // mechanical door
    mechanicalDoor: [/*{
        doorConfig: {
            startPosition: {
                x: startPixelX,
                y: startPixelY
            },
            endPosition: {
                x: endPixelX,
                y: endPixelY
            },
            moveVelocity: moveVelocity,
            imageName: imageName
        }, 
        buttonConfig: {
            startPosition: {
                x: startPixelX,
                y: startPixelY
            },
            endPosition: {
                x: endPixelX,
                y: endPixelY
            },
            imageName: imageName
        },
        interactionConfig: {
            p1_can_use: (true/false),
            p2_can_use: (true/false),
            blocks_can_use: (true/false)
        }
    }*/],
    // gateway
    gateway: [/*{
        doorConfig: {
            door1: {
                startPosition: {
                    x: startPixelX,
                    y: startPixelY
                },
                endPosition: {
                    x: endPixelX,
                    y: endPixelY
                },
                imageName: imageName,
            },
            door2: {
                startPosition: {
                    x: startPixelX,
                    y: startPixelY
                },
                endPosition: {
                    x: endPixelX,
                    y: endPixelY
                },
                imageName: imageName
            },
            animation_index_set: {
                idle: [index],
                working: [index],
                cooldown: [index]
            }
        },
        interactionConfig: {
            p1_can_use: (true/false),
            p2_can_use: (true/false),
            blocks_can_use: (true/false)
        }
    }*/],
    // floating platform
    floatingPlatform: [/*{
        platformConfig: {
            startPosition: {
                x: startPixelX,
                y: startPixelY
            },
            endPosition: {
                x: endPixelX,
                y: endPixelY
            },
            imageName: imageName,
            animation_index_set: {
                idle: [index],
                working: [index]
            }
        },
        buttonConfig: {
            startPosition: {
                x: startPixelX,
                y: startPixelY
            },
            endPosition: {
                x: endPixelX,
                y: endPixelY
            },
            imageName: imageName,
            animation_index_set: {
                button_up: [index],
                button_down: [index]
            }
        },
        moveConfig: {
            startPosition: {
                x: startPixelX,
                y: startPixelY
            },
            endPosition: {
                x: endPixelX,
                y: endPixelY
            },
            velocity: moveVelocity
        },
        interactionConfig: {
            p1_can_use: (true/false),
            p2_can_use: (true/false),
            blocks_can_use: (true/false)
        }
    }*/],
    // trap
    trap: [/*{
        trapConfig: {
            startPosition: {
                x: startPixelX,
                y: startPixelY
            },
            endPosition: {
                x: endPixelX,
                y: endPixelY5
            },
            imageName: 'trap_image'
        }
    }*/],
    // fountain
    fountain: [/*{
        config: {
            x: PixelX,
            y: PixelY,
            height: PixelHeight
        }
    }*/],
    // blocks
    blocks: [/*{
        config: {
            x: PixelX,
            y: PixelY,
            blockable: (true/false)
        }
    }*/],
    BN_Item: [/*{
        config: {
            x: PixelX,
            y: PixelY,
            type: (Z/C)
        }
    }, {
        config: {
            x: PixelX,
            y: PixelY,
            type: (Z/C)
        }
    }*/],
    Goal: {
        /*
        x: PixelX,
        y: PixelY
        */
    }
};