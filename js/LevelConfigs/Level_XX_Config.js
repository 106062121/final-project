var Level_XX = {
    level: 'Level_XX',
    bgm: '03',
    playerpos: {
        
        p1: {
            x: 4*tilesize,
            y: 35*tilesize
        },
        p2: {
            x: 6*tilesize,
            y: 35*tilesize
        }
        
        /*
        p1: {
            x: 41*tilesize,
            y: 30*tilesize
        },
        p2: {
            x: 41*tilesize,
            y: 30*tilesize
        }
        */
    },
    Story: [
        'Blue：你搞什麼啦，我們又被丟到奇怪的地方了。\nRed：抱歉啦，我真的對它們很沒抵抗力。',
        'Blue：那根羽毛又縮小了嗎？這讓我想到一些事情...\nRed：哇那個是什麼？！',
        'Blue：等等聽我說完啊...'
    ],
    // mechanical door
    mechanicalDoor: [{
        doorConfig: {
            startPosition: {
                x: 8*tilesize,
                y: 39*tilesize
            },
            endPosition: {
                x: 10*tilesize,
                y: 47*tilesize
            },
            moveVelocity: 50,
            imageName: 'blue_mechanical_door_1_4'
        }, 
        buttonConfig: {
            startPosition: {
                x: 0*tilesize,
                y: 46.5*tilesize
            },
            endPosition: {
                x: 2*tilesize,
                y: 47*tilesize
            },
            imageName: 'blue_mechanical_door_button'
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }, {
        doorConfig: {
            startPosition: {
                x: 10*tilesize,
                y: 39*tilesize
            },
            endPosition: {
                x: 12*tilesize,
                y: 47*tilesize
            },
            moveVelocity: 50,
            imageName: 'red_mechanical_door_1_4'
        }, 
        buttonConfig: {
            startPosition: {
                x: 9*tilesize,
                y: 36.5*tilesize
            },
            endPosition: {
                x: 11*tilesize,
                y: 37*tilesize
            },
            imageName: 'red_mechanical_door_button'
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: true,
            blocks_can_use: true
        }
    }, {
        doorConfig: {
            startPosition: {
                x: 73*tilesize,
                y: 39*tilesize
            },
            endPosition: {
                x: 75*tilesize,
                y: 45*tilesize
            },
            moveVelocity: 50,
            imageName: 'blue_mechanical_door_1_3'
        }, 
        buttonConfig: {
            startPosition: {
                x: 98*tilesize,
                y: 42.5*tilesize
            },
            endPosition: {
                x: 100*tilesize,
                y: 43*tilesize
            },
            imageName: 'blue_mechanical_door_button'
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }, {
        doorConfig: {
            startPosition: {
                x: 95*tilesize,
                y: 38*tilesize
            },
            endPosition: {
                x: 97*tilesize,
                y: 43*tilesize
            },
            moveVelocity: 50,
            imageName: 'red_mechanical_door_1_2'
        }, 
        buttonConfig: {
            startPosition: {
                x: 76*tilesize,
                y: 44.5*tilesize
            },
            endPosition: {
                x: 78*tilesize,
                y: 45*tilesize
            },
            imageName: 'red_mechanical_door_button'
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: true,
            blocks_can_use: true
        }
    }, {
        doorConfig: {
            startPosition: {
                x: 94*tilesize,
                y: 0*tilesize
            },
            endPosition: {
                x: 96*tilesize,
                y: 6*tilesize
            },
            moveVelocity: 50,
            imageName: 'red_mechanical_door_1_3'
        }, 
        buttonConfig: {
            startPosition: {
                x: 2*tilesize,
                y: 21.5*tilesize
            },
            endPosition: {
                x: 4*tilesize,
                y: 22*tilesize
            },
            imageName: 'red_mechanical_door_button'
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: true,
            blocks_can_use: true
        }
    }],
    // gateway
    gateway: [{
        doorConfig: {
            door1: {
                startPosition: {
                    x: 49*tilesize,
                    y: 30*tilesize
                },
                endPosition: {
                    x: 53*tilesize,
                    y: 36*tilesize
                },
                imageName: 'red_gateway',
            },
            door2: {
                startPosition: {
                    x: 29*tilesize,
                    y: 16*tilesize
                },
                endPosition: {
                    x: 33*tilesize,
                    y: 22*tilesize
                },
                imageName: 'red_gateway'
            },
            animation_index_set: {
                idle: [0, 1, 2],
                working: [0, 1, 2],
                cooldown: [0, 1, 2]
            }
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: true,
            blocks_can_use: true
        }
    }, {
        doorConfig: {
            door1: {
                startPosition: {
                    x: 40*tilesize,
                    y: 6*tilesize
                },
                endPosition: {
                    x: 44*tilesize,
                    y: 12*tilesize
                },
                imageName: 'purple_gateway',
            },
            door2: {
                startPosition: {
                    x: 0*tilesize,
                    y: 7*tilesize
                },
                endPosition: {
                    x: 4*tilesize,
                    y: 13*tilesize
                },
                imageName: 'purple_gateway'
            },
            animation_index_set: {
                idle: [0, 1, 2],
                working: [0, 1, 2],
                cooldown: [0, 1, 2]
            }
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: true,
            blocks_can_use: true
        }
    }],
    // floating platform
    floatingPlatform: [{
        platformConfig: {
            startPosition: {
                x: 20*tilesize,
                y: 47*tilesize
            },
            endPosition: {
                x: 24*tilesize,
                y: 49*tilesize
            },
            imageName: 'purple_floating_platform_4_1',
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 25*tilesize,
                y: 46.5*tilesize
            },
            endPosition: {
                x: 27*tilesize,
                y: 47*tilesize
            },
            imageName: 'purple_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 22*tilesize,
                y: 48*tilesize
            },
            endPosition: {
                x: 22*tilesize,
                y: 41*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: true,
            blocks_can_use: true
        }
    }, {
        platformConfig: {
            startPosition: {
                x: 41*tilesize,
                y: 32*tilesize
            },
            endPosition: {
                x: 45*tilesize,
                y: 34*tilesize
            },
            imageName: 'blue_floating_platform_4_1',
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 47*tilesize,
                y: 31.5*tilesize
            },
            endPosition: {
                x: 49*tilesize,
                y: 32*tilesize
            },
            imageName: 'blue_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 43*tilesize,
                y: 33*tilesize
            },
            endPosition: {
                x: 43*tilesize,
                y: 23*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }, {
        platformConfig: {
            startPosition: {
                x: 7*tilesize,
                y: 4*tilesize
            },
            endPosition: {
                x: 11*tilesize,
                y: 6*tilesize
            },
            imageName: 'blue_floating_platform_4_1',
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 13*tilesize,
                y: 12.5*tilesize
            },
            endPosition: {
                x: 15*tilesize,
                y: 13*tilesize
            },
            imageName: 'blue_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 9*tilesize,
                y: 5*tilesize
            },
            endPosition: {
                x: 57*tilesize,
                y: 22*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }, {
        platformConfig: {
            startPosition: {
                x: 5*tilesize,
                y: 13*tilesize
            },
            endPosition: {
                x: 10*tilesize,
                y: 15*tilesize
            },
            imageName: 'blue_floating_platform_5_1',
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 12*tilesize,
                y: 23.5*tilesize
            },
            endPosition: {
                x: 14*tilesize,
                y: 24*tilesize
            },
            imageName: 'blue_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 7.5*tilesize,
                y: 14*tilesize
            },
            endPosition: {
                x: 11.5*tilesize,
                y: 14*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }, {
        platformConfig: {
            startPosition: {
                x: 5*tilesize,
                y: 15*tilesize
            },
            endPosition: {
                x: 10*tilesize,
                y: 17*tilesize
            },
            imageName: 'blue_floating_platform_5_1',
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 12*tilesize,
                y: 23.5*tilesize
            },
            endPosition: {
                x: 14*tilesize,
                y: 24*tilesize
            },
            imageName: 'blue_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 7.5*tilesize,
                y: 16*tilesize
            },
            endPosition: {
                x: 3.5*tilesize,
                y: 16*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }, {
        platformConfig: {
            startPosition: {
                x: 78*tilesize,
                y: 47*tilesize
            },
            endPosition: {
                x: 94*tilesize,
                y: 49*tilesize
            },
            imageName: 'white_floating_platform_5_1',
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 68*tilesize,
                y: 44.5*tilesize
            },
            endPosition: {
                x: 70*tilesize,
                y: 45*tilesize
            },
            imageName: 'white_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 86*tilesize,
                y: 48*tilesize
            },
            endPosition: {
                x: 86*tilesize,
                y: 7*tilesize
            },
            velocity: 50
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: false,
            blocks_can_use: true
        }
    }],
    // trap
    trap: [{
        trapConfig: {
            startPosition: {
                x: 78*tilesize,
                y: 46*tilesize
            },
            endPosition: {
                x: 80*tilesize,
                y: 47*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 80*tilesize,
                y: 46*tilesize
            },
            endPosition: {
                x: 82*tilesize,
                y: 47*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 82*tilesize,
                y: 46*tilesize
            },
            endPosition: {
                x: 84*tilesize,
                y: 47*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 84*tilesize,
                y: 46*tilesize
            },
            endPosition: {
                x: 86*tilesize,
                y: 47*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 86*tilesize,
                y: 46*tilesize
            },
            endPosition: {
                x: 88*tilesize,
                y: 47*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 88*tilesize,
                y: 46*tilesize
            },
            endPosition: {
                x: 90*tilesize,
                y: 47*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 90*tilesize,
                y: 46*tilesize
            },
            endPosition: {
                x: 92*tilesize,
                y: 47*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 92*tilesize,
                y: 46*tilesize
            },
            endPosition: {
                x: 94*tilesize,
                y: 47*tilesize
            },
            imageName: 'trap_image'
        }
    }],
    // fountain
    fountain: [{
        config: {
            x: 85*tilesize,
            y: 31*tilesize,
            height: 400
        }
    }],
    // blocks
    blocks: [{
        config: {
            x: 0,
            y: 1000,
            blockable: false
        }
    }, {
        config: {
            x: 1080,
            y: 1000,
            blockable: false
        }
    }, {
        config: {
            x: 1440,
            y: 400,
            blockable: false
        }
    }, {
        config: {
            x: 360,
            y: 0,
            blockable: false
        }
    }, {
        config: {
            x: 360,
            y: 80,
            blockable: false
        }
    }, {
        config: {
            x: 1800,
            y: 1600,
            blockable: false
        }
    }, {
        config: {
            x: 2860,
            y: 1700,
            blockable: false
        }
    }],
    BN_Item: [],
    Goal: {
        x: 98*tilesize,
        y: 2*tilesize,
        imageName: "green_feather_160"
    },
    Torch: [{
        x: 97*tilesize, 
        y: 45*tilesize
    }, {
        x: 97*tilesize, 
        y: 27*tilesize
    }, {
        x: 97*tilesize, 
        y: 15*tilesize
    }, {
        x: 77*tilesize, 
        y: 37*tilesize
    }, {
        x: 73*tilesize, 
        y: 5*tilesize
    }, {
        x: 20*tilesize, 
        y: 14*tilesize
    }, {
        x: 18*tilesize, 
        y: 2*tilesize
    }, {
        x: 2*tilesize, 
        y: 48*tilesize
    }, {
        x: 10*tilesize, 
        y: 23*tilesize
    }, {
        x: 12.25*tilesize, 
        y: 34.25*tilesize
    }, {
        x: 38.25*tilesize, 
        y: 44.25*tilesize
    }, {
        x: 49.25*tilesize, 
        y: 16.25*tilesize
    }, {
        x: 59.25*tilesize, 
        y: 44.25*tilesize
    }, {
        x: 67.25*tilesize, 
        y: 25.25*tilesize
    }]
};