var Teaching_Level = {
    level: 'Teaching_Level',
    bgm: '04',
    //Player starting position
    playerpos: {
        
        p1: {
            x: 3*tilesize,
            y: 27*tilesize
        },
        p2: {
            x: 8*tilesize,
            y: 27*tilesize
        }
        
        /*
        p1: {
            x: 10*tilesize,
            y: 4*tilesize
        },
        p2: {
            x: 12*tilesize,
            y: 4*tilesize
        }
        */
    }, 
    Story: [
        'Red：好久了啊，到處都沒看到其他貓。\n路上到處都是奇怪的黑貓，\n一被看到就衝過來，嚇死我了',
        'Red：啊啊~~好想念住我隔壁的大姊姊，\n每天早上跟牠說完話，我一天的精神都來了。\n感覺還像一週前發生的事呢~~',
        'Blue：真是廢話，整個事件發生才不到一週呢，\n算了，估計你的大姊姊現在也不知道在路上哪裡遊魂，\n你還是顧好眼前的狀況吧。',
        'Blue：找了這麼久都沒有其他倖存者，\n甚至連事件的蛛絲馬跡都沒有找到，\n我們最後的希望只剩下城堡裡了',
        'Blue：要是連這裡也甚麼都沒有，\n我們可能就得拋棄這個國家了...',
        'Red：沒關係，不管到哪裡，我都會一~直陪著你，\n放心吧，你永遠不會感到寂寞的~~',
        'Blue：說...說甚麼肉麻的話\n，還是快點搜索城堡看看吧，\n我真的很不想捨棄這個我從小長大的地方。'
    ],
    // mechanical door
    mechanicalDoor: [{
        doorConfig: {
            startPosition: {
                x: 47*tilesize,
                y: 25*tilesize
            },
            endPosition: {
                x: 49*tilesize,
                y: 27*tilesize
            },
            moveVelocity: 50,
            imageName: 'blue_mechanical_door_1_1'
        }, 
        buttonConfig: {
            startPosition: {
                x: 49*tilesize,
                y: 20.5*tilesize
            },
            endPosition: {
                x: 51*tilesize,
                y: 21*tilesize
            },
            imageName: 'blue_mechanical_door_button'
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }, {
        doorConfig: {
            startPosition: {
                x: 51*tilesize,
                y: 15*tilesize
            },
            endPosition: {
                x: 53*tilesize,
                y: 21*tilesize
            },
            moveVelocity: 50,
            imageName: 'red_mechanical_door_1_3'
        }, 
        buttonConfig: {
            startPosition: {
                x: 49*tilesize,
                y: 26.5*tilesize
            },
            endPosition: {
                x: 51*tilesize,
                y: 27*tilesize
            },
            imageName: 'red_mechanical_door_button'
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: true,
            blocks_can_use: true
        }
    }],
    // gateway
    gateway: [{
        doorConfig: {
            door1: {
                startPosition: {
                    x: 70*tilesize,
                    y: 2*tilesize
                },
                endPosition: {
                    x: 74*tilesize,
                    y: 8*tilesize
                },
                imageName: 'red_gateway',
            },
            door2: {
                startPosition: {
                    x: 54*tilesize,
                    y: 3*tilesize
                },
                endPosition: {
                    x: 58*tilesize,
                    y: 9*tilesize
                },
                imageName: 'red_gateway'
            },
            animation_index_set: {
                idle: [0, 1, 2],
                working: [0, 1, 2],
                cooldown: [0, 1, 2]
            }
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: true,
            blocks_can_use: true
        }
    }],
    // floating platform
    floatingPlatform: [{
        platformConfig: {
            startPosition: {
                x: 68*tilesize,
                y: 25*tilesize
            },
            endPosition: {
                x: 72*tilesize,
                y: 27*tilesize
            },
            imageName: 'blue_floating_platform_2_1',
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 75*tilesize,
                y: 16.5*tilesize
            },
            endPosition: {
                x: 77*tilesize,
                y: 17*tilesize
            },
            imageName: 'blue_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 70*tilesize,
                y: 26*tilesize
            },
            endPosition: {
                x: 70*tilesize,
                y: 18*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    },{
        platformConfig: {
            startPosition: {
                x: 92*tilesize,
                y: 25*tilesize
            },
            endPosition: {
                x: 100*tilesize,
                y: 27*tilesize
            },
            imageName: 'blue_floating_platform_4_1',
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 84*tilesize,
                y: 26.5*tilesize
            },
            endPosition: {
                x: 86*tilesize,
                y: 27*tilesize
            },
            imageName: 'blue_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 96*tilesize,
                y: 26*tilesize
            },
            endPosition: {
                x: 96*tilesize,
                y: 9*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }],
    // trap
    trap: [{
        trapConfig: {
            startPosition: {
                x: 14*tilesize,
                y: 6*tilesize
            },
            endPosition: {
                x: 16*tilesize,
                y: 8*tilesize
            },
            imageName: 'trap_image'
        }
    }],
    // fountain
    fountain: [{
        config: {
            x: 24*tilesize,
            y: 7*tilesize,
            height: 400
        }
    }, {
        config: {
            x: 48*tilesize,
            y: 9*tilesize,
            height: 400
        }
    }],
    // blocks
    blocks: [{
        config: {
            x: 54*tilesize,
            y: 21*tilesize,
            blockable: false
        }
    }, {
        config: {
            x: 95*tilesize,
            y: 25*tilesize,
            blockable: false
        }
    }, {
        config: {
            x: 35*tilesize,
            y: 5*tilesize,
            blockable: true
        }
    }],
    BN_Item: [{
    }],
    Goal: {
        x: 2.5*tilesize,
        y: 4.5*tilesize,
        imageName: "red_feather_160"
    },
    Torch: [{
        x: 14*tilesize, 
        y: 1*tilesize
    }, {
        x: 34*tilesize, 
        y: 1*tilesize
    }, {
        x: 17*tilesize, 
        y: 14*tilesize
    }, {
        x: 35*tilesize, 
        y: 14*tilesize
    }, {
        x: 53*tilesize, 
        y: 14*tilesize
    }, {
        x: 67*tilesize, 
        y: 18*tilesize
    }, {
        x: 47*tilesize, 
        y: 22*tilesize
    }, {
        x: 89*tilesize, 
        y: 14*tilesize
    }, {
        x: 81*tilesize, 
        y: 3*tilesize
    }, {
        x: 64*tilesize, 
        y: 6*tilesize
    }]
};