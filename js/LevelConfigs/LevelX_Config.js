var LevelX = {
    level: 'LevelX',
    bgm: '04',
    //Player starting position
    playerpos: {
        
        p1: {
            x: 10*tilesize,
            y: 20*tilesize
        },
        p2: {
            x: 8*tilesize,
            y: 20*tilesize
        }
        
        /*
        p1: {
            x: 29*tilesize,
            y: 18*tilesize
        },
        p2: {
            x: 31*tilesize,
            y: 18*tilesize
        }
        */
    },
    Story: [
        'Red：好慘烈的景象...這裡到底發生過什麼事...\nBlue：根據這裡的紀錄來看，這個房間應該是實驗場，\n好像是用來實驗強化身體的藥...什麼？！',
        'Red：怎麼了？\nBlue：最後一批實驗記錄上，受試者全部失去理智，\n症狀跟街上的人們一模一樣...',
        'Red：你是說，這個並不是傳染病...\nBlue：看來是有人打算將藥外流，結果不小心在街上...\nRed：那個已經不重要啦，你看看對面',
        'Blue：那個是...第三根羽毛！！\nRed：沒錯，只要收集到那根羽毛，那大家都會沒事了，\n可是我們要怎麼通過這些尖刺呢?',
        'Blue：...\nRed：我看你這樣，應該是想到辦法但是不想說對吧？\nBlue：是啊，被你猜到了...',
        'Red：說出來吧，我會好好聽著的。\nBlue：...把我丟上去之後，你得喝下那瓶藥...\nRed：沒問題。',
        'Blue：？！...你真的知道會發生什麼事嗎？\nRed：我相信你已經想到解決辦法了。\nBlue：謝謝你相信我。',
        'Blue：你喝下藥之後，我會負責引導你（Press M）。\n這樣，你應該會朝我一直直走，但是靠不夠近不會跳，\n你不會受到尖刺的傷害，畢竟是強化身體的藥，\n',
        'Blue：雖然你的速度和跳躍力會下降，\n你可以試試掙扎一下（Press S），\n能夠跑、跳得比平常還快、還高...',
        'Red：好啦，你也太了解了吧。\n我大概了解了，如果你得吃藥的話，\n那我也會引導你的（Press B）。'
    ],
    // mechanical door
    mechanicalDoor: [],
    // gateway
    gateway: [],
    // floating platform
    floatingPlatform: [],
    // trap
    trap: [{
        trapConfig: {
            startPosition: {
                x: 12*tilesize,
                y: 21*tilesize
            },
            endPosition: {
                x: 14*tilesize,
                y: 23*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 14*tilesize,
                y: 21*tilesize
            },
            endPosition: {
                x: 16*tilesize,
                y: 23*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 16*tilesize,
                y: 21*tilesize
            },
            endPosition: {
                x: 18*tilesize,
                y: 23*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 18*tilesize,
                y: 21*tilesize
            },
            endPosition: {
                x: 20*tilesize,
                y: 23*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 20*tilesize,
                y: 21*tilesize
            },
            endPosition: {
                x: 22*tilesize,
                y: 23*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 22*tilesize,
                y: 21*tilesize
            },
            endPosition: {
                x: 24*tilesize,
                y: 23*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 24*tilesize,
                y: 21*tilesize
            },
            endPosition: {
                x: 26*tilesize,
                y: 23*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 26*tilesize,
                y: 21*tilesize
            },
            endPosition: {
                x: 28*tilesize,
                y: 23*tilesize
            },
            imageName: 'trap_image'
        }
    }],
    // fountain
    fountain: [],
    // blocks
    blocks: [],
    BN_Item: [{
        config: {
            x: 3*tilesize,
            y: 20*tilesize,
            type: 'Z'
        }
    }, {
        config: {
            x: 35*tilesize,
            y: 20*tilesize,
            type: 'C'
        }
    }],
    Goal: {
        x: 42.5*tilesize,
        y: 17.5*tilesize,
        imageName: "blue_feather_160"
    },
    Torch: [{
        x: 24*tilesize, 
        y: 6*tilesize
    }, {
        x: 44*tilesize, 
        y: 6*tilesize
    }, {
        x: 4*tilesize, 
        y: 6*tilesize
    }, {
        x: 17*tilesize, 
        y: 14*tilesize
    }, {
        x: 5*tilesize, 
        y: 21*tilesize
    }, {
        x: 35*tilesize, 
        y: 21*tilesize
    }]
};