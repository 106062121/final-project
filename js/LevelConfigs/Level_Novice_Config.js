var Level_Novice = {
    level: 'Level_Novice',
    bgm: '02',
    playerpos: {
        
        p1: {
            x: 5*tilesize,
            y: 20*tilesize
        },
        p2: {
            x: 3*tilesize,
            y: 20*tilesize
        }
        
        /*
        p1: {
            x: 35*tilesize,
            y: 13*tilesize
        },
        p2: {
            x: 33*tilesize,
            y: 13*tilesize
        }
        */
        /*
        p1: {
            x: 54*tilesize,
            y: 12*tilesize
        },
        p2: {
            x: 57*tilesize,
            y: 20*tilesize
        }
        */
        /*
        p1: {
            x: 93*tilesize,
            y: 12*tilesize
        },
        p2: {
            x: 95*tilesize,
            y: 12*tilesize
        }
        */
    },
    Story: [
        '（一小時之後，Blue先醒了過來。）\nBlue：這裡...還是城堡裡面吧？\n欸欸，Red，你快點起來啊。',
        'Red：嗯...，那根羽毛呢？跑去哪裡了？\nBlue：你懷裡發光的是什麼啊？！\nRed：是剛剛的羽毛ㄟ，可是縮小好多喔',
        'Blue：先不管這個了，這裡感覺不太安全，\n我們還是先想辦法離開吧...',
        'Red：那我先把這根羽毛收起來，\n感覺能保佑我們安全離開城堡呢~~'
    ],
    // mechanical door
    mechanicalDoor: [{
        doorConfig: {
            startPosition: {
                x: 43*tilesize,
                y: 3*tilesize
            },
            endPosition: {
                x: 45*tilesize,
                y: 11*tilesize
            },
            moveVelocity: 50,
            imageName: 'blue_mechanical_door_1_4'
        }, 
        buttonConfig: {
            startPosition: {
                x: 37*tilesize,
                y: 21.5*tilesize
            },
            endPosition: {
                x: 40*tilesize,
                y: 22*tilesize
            },
            imageName: 'blue_mechanical_door_button'
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }],
    // gateway
    gateway: [{
        doorConfig: {
            door1: {
                startPosition: {
                    x: 62*tilesize,
                    y: 13*tilesize
                },
                endPosition: {
                    x: 66*tilesize,
                    y: 19*tilesize
                },
                imageName: 'red_gateway',
            },
            door2: {
                startPosition: {
                    x: 76*tilesize,
                    y: 16*tilesize
                },
                endPosition: {
                    x: 80*tilesize,
                    y: 22*tilesize
                },
                imageName: 'red_gateway'
            },
            animation_index_set: {
                idle: [0, 1, 2],
                working: [0, 1, 2],
                cooldown: [0, 1, 2]
            }
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: true,
            blocks_can_use: true
        }
    }, {
        doorConfig: {
            door1: {
                startPosition: {
                    x: 88*tilesize,
                    y: 16*tilesize
                },
                endPosition: {
                    x: 92*tilesize,
                    y: 22*tilesize
                },
                imageName: 'blue_gateway',
            },
            door2: {
                startPosition: {
                    x: 96*tilesize,
                    y: 16*tilesize
                },
                endPosition: {
                    x: 100*tilesize,
                    y: 22*tilesize
                },
                imageName: 'blue_gateway'
            },
            animation_index_set: {
                idle: [0, 1, 2],
                working: [0, 1, 2],
                cooldown: [0, 1, 2]
            }
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }],
    // floating platform
    floatingPlatform: [{
        platformConfig: {
            startPosition: {
                x: 19*tilesize,
                y: 11*tilesize
            },
            endPosition: {
                x: 29*tilesize,
                y: 13*tilesize
            },
            imageName: 'white_floating_platform_5_1',
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 2*tilesize,
                y: 9.5*tilesize
            },
            endPosition: {
                x: 4*tilesize,
                y: 10*tilesize
            },
            imageName: 'white_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 24*tilesize,
                y: 12*tilesize
            },
            endPosition: {
                x: 24*tilesize,
                y: 14*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: false,
            blocks_can_use: true
        }
    }, {
        platformConfig: {
            startPosition: {
                x: 47*tilesize,
                y: 19*tilesize
            },
            endPosition: {
                x: 51*tilesize,
                y: 21*tilesize
            },
            imageName: 'red_floating_platform_2_1',
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 56*tilesize,
                y: 21.5*tilesize
            },
            endPosition: {
                x: 58*tilesize,
                y: 22*tilesize
            },
            imageName: 'red_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 49*tilesize,
                y: 20*tilesize
            },
            endPosition: {
                x: 49*tilesize,
                y: 12*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: false,
            p2_can_use: true,
            blocks_can_use: true
        }
    }, {
        platformConfig: {
            startPosition: {
                x: 81*tilesize,
                y: 20*tilesize
            },
            endPosition: {
                x: 87*tilesize,
                y: 22*tilesize
            },
            imageName: 'blue_floating_platform_3_1',
            animation_index_set: {
                idle: [0],
                working: [1, 2]
            }
        },
        buttonConfig: {
            startPosition: {
                x: 100*tilesize,
                y: 23.5*tilesize
            },
            endPosition: {
                x: 102*tilesize,
                y: 24*tilesize
            },
            imageName: 'blue_floating_platform_button',
            animation_index_set: {
                button_up: [0],
                button_down: [1]
            }
        },
        moveConfig: {
            startPosition: {
                x: 84*tilesize,
                y: 21*tilesize
            },
            endPosition: {
                x: 84*tilesize,
                y: 15*tilesize
            },
            velocity: 100
        },
        interactionConfig: {
            p1_can_use: true,
            p2_can_use: false,
            blocks_can_use: true
        }
    }],
    // trap
    trap: [{
        trapConfig: {
            startPosition: {
                x: 16*tilesize,
                y: 20*tilesize
            },
            endPosition: {
                x: 18*tilesize,
                y: 22*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 18*tilesize,
                y: 20*tilesize
            },
            endPosition: {
                x: 20*tilesize,
                y: 22*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 20*tilesize,
                y: 20*tilesize
            },
            endPosition: {
                x: 22*tilesize,
                y: 22*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 22*tilesize,
                y: 20*tilesize
            },
            endPosition: {
                x: 24*tilesize,
                y: 22*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 24*tilesize,
                y: 20*tilesize
            },
            endPosition: {
                x: 26*tilesize,
                y: 22*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 26*tilesize,
                y: 20*tilesize
            },
            endPosition: {
                x: 28*tilesize,
                y: 22*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 28*tilesize,
                y: 20*tilesize
            },
            endPosition: {
                x: 30*tilesize,
                y: 22*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 30*tilesize,
                y: 20*tilesize
            },
            endPosition: {
                x: 32*tilesize,
                y: 22*tilesize
            },
            imageName: 'trap_image'
        }
    }
     
     , {
        trapConfig: {
            startPosition: {
                x: 60*tilesize,
                y: 9*tilesize
            },
            endPosition: {
                x: 62*tilesize,
                y: 11*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 62*tilesize,
                y: 9*tilesize
            },
            endPosition: {
                x: 64*tilesize,
                y: 11*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 64*tilesize,
                y: 9*tilesize
            },
            endPosition: {
                x: 66*tilesize,
                y: 11*tilesize
            },
            imageName: 'trap_image'
        }
    }, {
        trapConfig: {
            startPosition: {
                x: 66*tilesize,
                y: 9*tilesize
            },
            endPosition: {
                x: 68*tilesize,
                y: 11*tilesize
            },
            imageName: 'trap_image'
        }
    }],
    // fountain
    fountain: [/*{
        config: {
            x: PixelX,
            y: PixelY,
            height: PixelHeight
        }
    }*/],
    // blocks
    blocks: [{
        config: {
            x: 7*tilesize,
            y: 10*tilesize,
            blockable: false
        }
    }, {
        config: {
            x: 62*tilesize,
            y: 5*tilesize,
            blockable: false
        }
    }],
    BN_Item: [/*{
        config: {
            x: PixelX,
            y: PixelY,
            type: (Z/C)
        }
    }, {
        config: {
            x: PixelX,
            y: PixelY,
            type: (Z/C)
        }
    }*/],
    Goal: {
        x: 100.5*tilesize,
        y: 11.5*tilesize,
        imageName: "yellow_feather_160"
    },
    Torch: [{
        x: 4*tilesize, 
        y: 6*tilesize
    }, {
        x: 6*tilesize, 
        y: 15*tilesize
    }, {
        x: 24*tilesize, 
        y: 6*tilesize
    }, {
        x: 24*tilesize, 
        y: 15*tilesize
    }, {
        x: 46*tilesize, 
        y: 6*tilesize
    }, {
        x: 43*tilesize, 
        y: 16*tilesize
    }, {
        x: 66*tilesize, 
        y: 6*tilesize
    }, {
        x: 59*tilesize, 
        y: 15*tilesize
    }, {
        x: 70*tilesize, 
        y: 17*tilesize
    }, {
        x: 96*tilesize, 
        y: 6*tilesize
    }, {
        x: 85*tilesize, 
        y: 15*tilesize
    }]
};