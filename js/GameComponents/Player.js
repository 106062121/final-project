_state.createPlayer = function () {
    this.players = game.add.group();
    this.p1 = game.add.sprite(this.globalConfig.playerpos.p1.x, this.globalConfig.playerpos.p1.y, 'pixie', 0, this.players);
    this.p2 = game.add.sprite(this.globalConfig.playerpos.p2.x, this.globalConfig.playerpos.p2.y, 'pixia', 0, this.players);

    this.setPlayerProperties(this.p1);
    this.setPlayerProperties(this.p2);

    this.addPlayerAnimations(this.p1);
    this.addPlayerAnimations(this.p2);

    //arrow object for p1 to aim
    this.arrow = game.add.sprite(this.p1.x, this.p1.y - this.p2.sprite, 'shoot');
    this.arrow.anchor.setTo(0.5, 1.5);
    this.arrow.visible = false;
}

_state.setPlayerProperties = function (player) {
    player.anchor.setTo(0.5, 1);
    game.physics.arcade.enable(player);
    //player.body.maxVelocity.x = game.global.maxVelocity;
    player.body.gravity.y = game.global.gravity.normal;
    player.body.collideWorldBounds = true;

    //Special move variable
    player.flying = false;
    player.blasting = false;
    player.zombinize = false;
    player.baiting = false;
    player.onboard = false;
}

_state.addPlayerAnimations = function (player) {
    player.animations.add('idle', [0, 1, 2, 3], 2, true);
    player.animations.add('walking', [4, 5, 6, 7, 8], 8, true);
    player.animations.add('interact', [1, 2], 16, true);
    player.animations.add('baiting', [1, 2, 3], 8, true);
}

_state.animationHandler = function (player) {
    if(player.body.velocity.x > 0) {

        player.scale.x = 1;
        player.animations.play('walking');

    }
    else if(player.body.velocity.x < 0) {

        player.scale.x = -1;
        player.animations.play('walking');

    }
    else if(player.baiting) {

        player.animations.play('baiting');
        
    }
    else {

        if(!player.body.onWall())
            player.animations.play('idle');

    }
}

_state.createLesson = function() {
    this.ctrl1 = game.add.sprite(3*40, 23*40-32, 'P1ctrl');
    this.ctrl1.anchor.setTo(0.3, 0.5);

    this.ctrl2 = game.add.sprite(8*40, 23*40-32, 'P2ctrl');
    this.ctrl2.anchor.setTo(0.5, 0.5);

    this.mech = game.add.sprite(42*40, 22*40, 'Mech');
    this.mech.alpha = 0;

    this.mechbox = game.add.sprite(48*40, 19*40, 'MechBox');
    this.mechbox.anchor.setTo(0, 1);
    this.mechbox.alpha = 0;

    this.shoot = game.add.sprite(65*40, 24*40, 'Shoot');
    this.shoot.anchor.setTo(0, 1);
    this.shoot.alpha = 0;

    this.plat = game.add.sprite(78*40, 13*40, 'Plat');
    this.plat.anchor.setTo(0.5, 1);
    this.plat.alpha = 0;

    this.bom = game.add.sprite(93*40, 24*40, 'Boom');
    this.bom.anchor.setTo(0.5, 1);
    this.bom.alpha = 0;

    this.gate = game.add.sprite(76*40, 5*40, 'Gate');
    this.gate.anchor.setTo(0.5, 1);
    this.gate.alpha = 0;

    this.fount = game.add.sprite(52*40, 5*40, 'Fount');
    this.fount.anchor.setTo(0.5, 1);
    this.fount.alpha = 0;

    this.fountbox = game.add.sprite(31*40, 3*40, 'Fountbox');
    this.fountbox.anchor.setTo(0.5, 1);
    this.fountbox.alpha = 0;

    this.dd = game.add.sprite(15*40, 3*40, 'Dead');
    this.dd.anchor.setTo(0.5, 1);
    this.dd.alpha = 0;

    this.ct1dis = game.add.tween(this.ctrl1).to({alpha:0}, 500, 'Linear');
    this.ct2dis = game.add.tween(this.ctrl2).to({alpha:0}, 500, 'Linear');

    this.mecapr = game.add.tween(this.mech).to({alpha:1}, 500, 'Linear');
    this.mecdis = game.add.tween(this.mech).to({alpha:0}, 500, 'Linear');

    this.mecbapr = game.add.tween(this.mechbox).to({alpha:1}, 500, 'Linear');
    this.mecbdis = game.add.tween(this.mechbox).to({alpha:0}, 500, 'Linear');

    this.shotapr = game.add.tween(this.shoot).to({alpha:1}, 500, 'Linear');
    this.shotdis = game.add.tween(this.shoot).to({alpha:0}, 500, 'Linear');

    this.pltapr = game.add.tween(this.plat).to({alpha:1}, 500, 'Linear');
    this.pltdis = game.add.tween(this.plat).to({alpha:0}, 500, 'Linear');

    this.bomapr = game.add.tween(this.bom).to({alpha:1}, 500, 'Linear');
    this.bomdis = game.add.tween(this.bom).to({alpha:0}, 500, 'Linear');

    this.gateapr = game.add.tween(this.gate).to({alpha:1}, 500, 'Linear');
    this.gatedis = game.add.tween(this.gate).to({alpha:0}, 500, 'Linear');

    this.fountapr = game.add.tween(this.fount).to({alpha:1}, 500, 'Linear');
    this.fountdis = game.add.tween(this.fount).to({alpha:0}, 500, 'Linear');

    this.fountbapr = game.add.tween(this.fountbox).to({alpha:1}, 500, 'Linear');
    this.fountbdis = game.add.tween(this.fountbox).to({alpha:0}, 500, 'Linear');

    this.ddapr = game.add.tween(this.dd).to({alpha:1}, 500, 'Linear');
    this.dddis = game.add.tween(this.dd).to({alpha:0}, 500, 'Linear');
}

_state.animationDetect = function() {
    if(this.globalConfig == Teaching_Level){
        //plaer control
        if(this.p1.x > 12*40){
            this.ct1dis.start();
        }
        else this.ctrl1.alpha = 1;

        if(this.p2.x > 12*40){
            this.ct2dis.start();
        }
        else this.ctrl2.alpha = 1;

        //mechnial door
        if( (this.p1.x < 39*40 || this.p1.x > 50*40) && (this.p2.x < 39*40 || this.p2.x > 50*40) ){
            this.mecdis.start();
        }
        else if( (this.p1.x >= 39*40 && this.p1.x <= 50*40) || (this.p2.x >= 39*40 && this.p2.x <= 50*40)){
            this.mecapr.start();
        }

        if(this.p1.x < this.mechbox.x-80 || this.p1.x > this.mechbox.x + 80 + this.mechbox.width){
            this.mecbdis.start();
        }
        else if(this.p1.x >= this.mechbox.x-80 && this.p1.x <= this.mechbox.x + 80 + this.mechbox.width && this.p1.y/40 <= 24){
            this.mecbapr.start();
        }

        if( this.p1.x < 62*40 || this.p1.x > 71*40 ){
            this.shotdis.start();
        }
        else if( this.p1.x > 62*40 && this.p1.x < 71*40 ){
            this.shotapr.start();
        }

        if( (this.p1.x < 73*40 || this.p1.x > 83*40) && (this.p2.x < 73*40 || this.p2.x > 83*40) ){
            this.pltdis.start();
        }
        else if( (this.p1.x >= 73*40 && this.p1.x <= 83*40) || (this.p2.x >= 73*40 && this.p2.x <= 83*40)){
            this.pltapr.start();
        }

        if( (this.p1.x < 90*40 || this.p1.x > 95*40) && (this.p2.x < 90*40 || this.p2.x > 95*40) ){
            this.bomdis.start();
        }
        else if( (this.p1.x >= 90*40 && this.p1.x <= 95*40) || (this.p2.x >= 90*40 && this.p2.x <= 95*40)){
            this.bomapr.start();
        }

        if( (this.p1.x < 71*40 || this.p1.x > 80*40) && (this.p2.x < 71*40 || this.p2.x > 80*40) ){
            this.gatedis.start();
        }
        else if( (this.p1.x >= 71*40 && this.p1.x <= 80*40) || (this.p2.x >= 71*40 && this.p2.x <= 80*40)){
            this.gateapr.start();
        }

        if( (this.p1.x < 49*40 || this.p1.x > 55*40) && (this.p2.x < 49*40 || this.p2.x > 55*40) ){
            this.fountdis.start();
        }
        else if( (this.p1.x >= 49*40 && this.p1.x <= 55*40) || (this.p2.x >= 49*40 && this.p2.x <= 55*40)){
            this.fountapr.start();
        }

        if( (this.p1.x < 25*40 || this.p1.x > 38*40) && (this.p2.x < 25*40 || this.p2.x > 38*40) ){
            this.fountbdis.start();
        }
        else if( (this.p1.x >= 25*40 && this.p1.x <= 38*40) || (this.p2.x >= 25*40 && this.p2.x <= 38*40)){
            this.fountbapr.start();
        }

        if( (this.p1.x < 12*40 || this.p1.x > 19*40) && (this.p2.x < 12*40 || this.p2.x > 19*40) ){
            this.dddis.start();
        }
        else if( (this.p1.x >= 12*40 && this.p1.x <= 19*40) || (this.p2.x >= 12*40 && this.p2.x <= 19*40)){
            this.ddapr.start();
        }
    }
}

