_state.createPauseElements = function () {
    this.pauseKey = game.input.keyboard.addKey(Phaser.Keyboard.P);
    this.pauseKey.onDown.add(function () {
        this.playFX('buttonClick');
        this.pause();
    }, this)

    this.pauseLabel = game.add.text(game.width/2, 200, 'Game Paused.', { font: '30px Arial', fill: '#ffffff' });
    this.pauseLabel.anchor.setTo(0.5, 0.5);
    this.pauseLabel.fixedToCamera = true;
    this.pauseLabel.kill();

    this.resumeLabel = game.add.text(game.width/2, 250, 'resume', { font: '25px Arial', fill: '#ffffff' });
    this.setLabelProperties(this.resumeLabel, { font: '25px Arial', fill: '#ffffff' }, { font: '25px Arial', fill: '#000000' }, this.pause);

    this.restartLabel = game.add.text(game.width/2, 290, 'restart', { font: '25px Arial', fill: '#ffffff' });
    this.setLabelProperties(this.restartLabel, { font: '25px Arial', fill: '#ffffff' }, { font: '25px Arial', fill: '#000000' }, this.restartGame);

    this.menuLabel = game.add.text(game.width/2, 330, 'go to menu', { font: '25px Arial', fill: '#ffffff' });
    this.setLabelProperties(this.menuLabel, { font: '25px Arial', fill: '#ffffff' }, { font: '25px Arial', fill: '#000000' }, this.goMenu);

    this.titleLabel = game.add.text(game.width/2, 370, 'back to title', { font: '25px Arial', fill: '#ffffff' });
    this.setLabelProperties(this.titleLabel, { font: '25px Arial', fill: '#ffffff' }, { font: '25px Arial', fill: '#000000' }, this.goTitle);
}

_state.setLabelProperties = function (Label, style, alter_style, callback) {
    Label.anchor.setTo(0.5, 0.5);
    Label.fixedToCamera = true;
    Label.inputEnabled = true;
    Label.events.onInputOver.add(function () {
        Label.setStyle(alter_style);
    }, this);
    Label.events.onInputOut.add(function () {
        Label.setStyle(style);
    }, this);
    Label.events.onInputDown.add(function () {
        this.playFX('buttonClick');
        callback.call(this);
    }, this);
    Label.kill();
}

_state.goMenu = function () {
    this.pause();
    game.state.start('menu');
}

_state.goTitle = function () {
    this.pause();
    game.state.start('load');
}

_state.createSoundEffects = function () {
    this.soundFX = {};

    this.soundFX['blockDrop'] = game.add.audio('blockDrop');
    this.soundFX['blockMove'] = game.add.audio('blockMove');
    this.soundFX['BN_cure'] = game.add.audio('BN_cure');
    this.soundFX['BN_zombie'] = game.add.audio('BN_zombie');
    this.soundFX['buttonClick'] = game.add.audio('buttonClick');
    this.soundFX['floatingPlatformMove'] = game.add.audio('floatingPlatformMove');
    this.soundFX['floatingPlatformTrigger'] = game.add.audio('floatingPlatformTrigger');
    this.soundFX['fountain'] = game.add.audio('fountain');
    this.soundFX['gateway'] = game.add.audio('gateway');
    this.soundFX['mechanicalDoorMove'] = game.add.audio('mechanicalDoorMove');
    this.soundFX['mechanicalDoorTrigger'] = game.add.audio('mechanicalDoorTrigger');
    this.soundFX['trap'] = game.add.audio('trap');
    this.soundFX['ZaMao'] = game.add.audio('ZaMao');
    this.soundFX['win'] = game.add.audio('win');
    this.soundFX['fail'] = game.add.audio('fail');
}

_state.playFX = function (key) {
    this.soundFX[key].play();
}

_state.stopFX = function (key) {
    this.soundFX[key].stop();
}

_state.detectMechanismFX = function() {
    var mechanical_door_total_doorMoving = false;
    this.mechanical_door.forEach(function(element) {
        if(element.doorMoving == true) {
            mechanical_door_total_doorMoving = true;
        }
    }, this);

    if(mechanical_door_total_doorMoving && !this.mechanicalDoorMoveFXplaying) {
        _state.playFX('mechanicalDoorMove');
        this.mechanicalDoorMoveFXplaying = true;
    }
    if(!mechanical_door_total_doorMoving && this.mechanicalDoorMoveFXplaying) {
        _state.stopFX('mechanicalDoorMove');
        this.mechanicalDoorMoveFXplaying = false;
    }

    var floating_platform_total_platformMoving = false;
    this.floating_platform.forEach(function(element) {
        if(element.platformMoving == true) {
            floating_platform_total_platformMoving = true;
        }
    }, this);

    if(floating_platform_total_platformMoving && !this.floatingPlatformMoveFXplaying) {
        _state.playFX('floatingPlatformMove');
        this.floatingPlatformMoveFXplaying = true;
    }
    if(!floating_platform_total_platformMoving && this.floatingPlatformMoveFXplaying) {
        _state.stopFX('floatingPlatformMove');
        this.floatingPlatformMoveFXplaying = false;
    }
}