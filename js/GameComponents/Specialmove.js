_state._interact = function (context, player) {
    //If p1 or p2 is zombinized, disable interact.
    if(this.p1.zombinize || this.p2.zombinize){
        return;
    }

    if (this.arrow.visible) {
        this.p1.body.gravity.y = game.global.gravity.normal;
        this.arrow.visible = false;
        this.p2.loadTexture('pixia');
        this.p2.body.height = this.p2.height;
    }
    else {
        if (this.interacting) {
            this.arrow.visible = true;
            this.p1.body.gravity.y = 0;
            this.p2.loadTexture('pixia2');
            this.p2.body.height = this.p2.height;
        }
    }
}

_state._eject = function (context, player) {
    if (this.arrow.visible) {
        var angle = -(this.arrow.angle-90) / 180 * Math.PI;
        this.p1.body.velocity.setTo(game.global.velocity.projectile * Math.cos(angle), -game.global.velocity.projectile * Math.sin(angle));
        this.p1.body.gravity.y = game.global.gravity.fly;
        this.p1.flying = true;
        this.arrow.visible = false;
        this.p2.loadTexture('pixia');
        this.p2.body.height = this.p2.height;
    }
}

_state._fly = function () {
    if (this.arrow.visible) {
        if (this.p1Control.left.isDown && this.arrow.angle-90 > -180)
            this.arrow.angle -= 1;
        else if (this.p1Control.right.isDown && this.arrow.angle-90 < 0)
            this.arrow.angle += 1;

        this.p1.x = this.p2.x;
        this.p1.y = this.p2.y - Math.abs(this.p1.height);
        this.arrow.x = this.p1.x;
        this.arrow.y = this.p1.y - Math.abs(this.p2.width / 2);
    }
}

_state.ZaMao = function(ctrl, player) {
    if(player == this.p2){
        if(ctrl){
            if (!this.soundFX['ZaMao'].isPlaying) {
                this.playFX('ZaMao');
            }
            player.blasting = true;
            player.loadTexture('bigia');
            player.body.width = Math.abs(player.width);
            player.body.height = player.height;
        }
        else if(player.key == 'bigia'){
            player.blasting = false;
            player.loadTexture('pixia');
            player.body.width = Math.abs(player.width);
            player.body.height = player.height;
        }
    }
    else {
        if(ctrl){
            if (!this.soundFX['ZaMao'].isPlaying) {
                this.playFX('ZaMao');
            }
            player.blasting = true;
            player.loadTexture('bigie');
            player.body.width = Math.abs(player.width);
            player.body.height = player.height;
        }
        else if(player.key == 'bigie'){
            player.blasting = false;
            player.loadTexture('pixie');
            player.body.width = Math.abs(player.width);
            player.body.height = player.height;
        }
    }
}

_state.Blockfly = function(player, block) {
    if(player.blasting){
        var x = block.x-player.x;
        var y = block.y-player.y;
        var dist = Math.sqrt(Math.pow(Math.abs(x),2)+Math.pow(Math.abs(y),2));
        block.body.velocity.x = 700*x/dist;
        block.body.velocity.y = 700*y/dist;
    }
}

_state.Zombinize = function(player, item) {
    if(!player.zombinize){
        item.kill();
        this.playFX('BN_zombie');
        player.loadTexture('enemy');
        player.body.width =  Math.abs(player.width);
        player.body.height = player.height;
        player.zombinize = true;
    }
}

_state.Revive = function(player,item) {
    if(player.zombinize){
        item.kill();
        this.playFX('BN_cure');
        if(player == this.p1){
            player.loadTexture('pixie');
        }
        else {
            player.loadTexture('pixia');
        }
        player.zombinize = false;
    }
}

_state.Zombiemove = function(ctrl, zombie) {
    var normie;
    var x = game.global.velocity.zombiex;
    var y = game.global.velocity.zombiey
    if(zombie == this.p1)normie = this.p2;
    else normie = this.p1;

    if(normie.baiting){
        if(normie.y < zombie.y && Math.abs(normie.body.x-zombie.body.x)<= Math.abs(normie.body.width*1.5) && (zombie.body.onFloor() || zombie.body.touching.down)){
            if(ctrl.isDown){
                zombie.body.velocity.y = -y;
            }
            else {
                zombie.body.velocity.y = -y/2;
            }
        }
        else if(normie.x > zombie.x && Math.abs(normie.x-zombie.x)>20){
            if(ctrl.isDown){
                zombie.body.velocity.x = x;
            }
            else {
                zombie.body.velocity.x = x/2;
            }
        }
        else if(normie.x < zombie.x && Math.abs(normie.x-zombie.x)>20){
            if(ctrl.isDown){
                zombie.body.velocity.x = -x;
            }
            else {
                zombie.body.velocity.x = -x/2;
            }
        }
    }
}

