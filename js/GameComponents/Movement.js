_state.createPlayerControls = function () {
    this.p1Control = game.input.keyboard.createCursorKeys();
    this.p1Control.interact = game.input.keyboard.addKey(Phaser.KeyCode.G)
    this.p1Control.eject = game.input.keyboard.addKey(Phaser.KeyCode.L)
    this.p1Control.ZM = game.input.keyboard.addKey(Phaser.KeyCode.K)
    this.p1Control.BT = game.input.keyboard.addKey(Phaser.KeyCode.M)

    this.p2Control = game.input.keyboard.addKeys( { 'up': Phaser.KeyCode.W,
                                                    'down': Phaser.KeyCode.S,
                                                    'left': Phaser.KeyCode.A,
                                                    'right': Phaser.KeyCode.D,
                                                    'interact': Phaser.KeyCode.G,
                                                    'eject': Phaser.KeyCode.L,
                                                    'ZM': Phaser.KeyCode.H,
                                                    'BT': Phaser.KeyCode.B } );

    this.createKeyListeners(this.p1, this.p1Control);
    this.createKeyListeners(this.p2, this.p2Control);
}

_state.createKeyListeners = function (player, ctrl) {
    ctrl.interact.onDown.add(this._interact, this, 0, player);
    ctrl.eject.onDown.add(this._eject, this, 0, player);
}

_state.movementHandler = function (player, ctrl) {
    if(player.zombinize){ //If player zombinized, run the function.
        this.Zombiemove(ctrl.down, player);
        return;
    }

    if (!player.flying) {
        if(ctrl.BT.isDown && (player.body.onFloor() || player.body.touching.down)){
            player.baiting = true;
            return;
        }
        else if(player.baiting)player.baiting = false;

        if (ctrl.left.isDown) {
            this._walk_left(player);
        }
    
        if (ctrl.right.isDown) {
            this._walk_right(player);
        }

        if (ctrl.up.isDown) {
            this._jump(player)
        }

        this.ZaMao(ctrl.ZM.isDown, player);
    }
}

_state._walk_left = function (player) {
    if (player.body.onFloor()) {
        player.body.velocity.x = -game.global.velocity.x;
    }
    else if (player.body.onWall()) {
        player.body.velocity.x = 0;
    }
    else {
        player.body.velocity.x = (player.body.velocity.x > 0) ? -game.global.velocity.inAir :
                                (player.body.velocity.x <= 0) ? -game.global.velocity.x : player.body.velocity.x;
    }
}

_state._walk_right = function (player) {
    if (player.body.onFloor()) {
        player.body.velocity.x = game.global.velocity.x;
    }
    else if (player.body.onWall()) {
        player.body.velocity.x = 0;
    }
    else {
        player.body.velocity.x = (player.body.velocity.x < 0) ? game.global.velocity.inAir :
                                (player.body.velocity.x >= 0) ? game.global.velocity.x : player.body.velocity.x;
    }
}

_state._jump = function (player) {
    if ((player.body.onFloor() || player.body.touching.down) && !player.zombinize && !player.baiting) {
        if (player.body.velocity.y && this.interacting && !player.onboard) {
            return;
        }
        player.body.velocity.y = -game.global.velocity.y;
    }
}

_state._drag = function () {
    this.blocks.forEachAlive(function (element) {
        if (element.body.onFloor() || element.body.touching.down) {
            element.body.drag.x = game.global.drag.block;
        }
        else {
            element.body.drag.x = 10;
        }
    }, this);

    if (this.p1.body.onFloor()) {
        this.p1.body.drag.x = game.global.drag.onFloor;
    }
    else if (this.arrow.visible) {
        this.p1.body.drag.x = game.global.drag.onFloor;
    }
    else if (this.p1.flying) {
        this.p1.body.drag.x = game.global.drag.flying;
    }
    else {
        this.p1.body.drag.x = game.global.drag.inAir;
    }

    if (this.p2.body.onFloor()) {
        this.p2.body.drag.x = game.global.drag.onFloor;
    }
    else {
        this.p2.body.drag.x = game.global.drag.inAir;
    }
}

_state._blockmove = function (blocks) {
    blocks.forEachAlive(function (element) {
        if (element.body.touching.down) {
            if (this.p1.body.touching.up) {
                if (this.p1Control.left.isDown || this.p1Control.right.isDown) {
                    element.body.velocity.x = this.p1.body.velocity.x;
                }
                else {
                    element.body.velocity.x = 0;
                }
            }
            else if (this.p2.body.touching.up) {
                if (this.p2Control.left.isDown || this.p2Control.right.isDown) {
                    element.body.velocity.x = this.p2.body.velocity.x;
                }
                else {
                    element.body.velocity.x = 0;
                }
            }
        }
        
    }, this)
}