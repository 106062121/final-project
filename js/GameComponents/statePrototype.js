var _state = {
    init: function (config) {
        this.globalConfig = config;
        game.sound.stopAll(); 
        this.bgm = game.add.audio('bgm' + config.bgm);
        this.bgm.play();
    },
    preload: function() {
        // tile assets
        game.load.tilemap('map', 'assets/'+this.globalConfig.level+'/tilemap.json', null, Phaser.Tilemap.TILED_JSON);
    },
    create: function() {

        /** basics **/

        this.timer = 0;

        this.createMap();
        this.createMapElements();
        this.createSoundEffects();

        this.createPlayer();
        this.createPlayerControls();
        
        this.createCamera();

        if(this.globalConfig == Teaching_Level){
            this.createLesson();
        }

        this.createTextWindow();
        this.startStory(this.globalConfig.Story);
        this.createPauseElements();
    },
    update: function() {
        this.timer += 1;

        // collisions
        this.collisionHandler();

        // movement
        this.movementHandler(this.p1, this.p1Control);
        this.movementHandler(this.p2, this.p2Control);
        this._fly();

        // collisions
        this.collisionHandler();
        this._drag();

        // moves camera
        this.updateCamera();

        // animations
        this.animationHandler(this.p1);
        this.animationHandler(this.p2);

        this.animationDetect();

        this.statecheck();

        this.detectMechanismFX();
    },
    pause: function () {
        if (!this.tellstory) {
            if (game.paused) {
                game.paused = false;
    
                this.pauseLabel.kill();
                this.resumeLabel.kill();
                this.restartLabel.kill();
                this.menuLabel.kill();
                this.titleLabel.kill();
                //this.scale_box.style.display = "none";
            }
            else {
                game.paused = true;
                
                this.pauseLabel.revive();
                this.resumeLabel.revive();
                this.restartLabel.revive();
                this.menuLabel.revive();
                this.titleLabel.revive();
                //this.scale_box.style.display = "block";
            }
        }
    },
    restartGame: function() {
        //game.state.restart(true, false, _config);
        this.playFX('fail');
        game.paused = false;
        game.state.start('debug', true, false, this.globalConfig);
    },
    statecheck: function() {
        if(this.p1reach && this.p2reach){
            this.playFX('win');
            if(this.globalConfig.level == 'Teaching_Level') {
                this.startStory(Level_Teaching_end);
                game.state.start('debug', true, false, Level_Novice);
            }
            else if(this.globalConfig.level == 'Level_Novice') {
                this.startStory(Level_Novice_end);
                game.state.start('debug', true, false, Level_XX);
            }
            else if(this.globalConfig.level == 'Level_XX') {
                this.startStory(Level_XX_end);
                game.state.start('debug', true, false, LevelX);
            }
            else if(this.globalConfig.level == 'LevelX') {
                this.startStory(Level_X_end);
                game.state.start('debug', true, false, Level_LIU);
            }
            else if(this.globalConfig.level == 'Level_LIU') {
                this.startStory(Level_XXX_end);
                console.log("Levels All Complete !!!");
                game.state.start('menu');
            }
        }
    }
};
