_state.createMap = function () {
    this.mapBG = game.add.tilemap('mapBG');

    this.mapBG.addTilesetImage('background_v1.2');
    this.bg_layer = this.mapBG.createLayer('bg');

    this.map = game.add.tilemap('map');

    this.map.addTilesetImage('terrain_tileset');
    this.map.addTilesetImage('tilemap_v1.3');
    
    this.terrain_layer = this.map.createLayer('TerrainLayer');
    var terrain_tileset_collision_array_from = [0];
    var terrain_tileset_collision_array_to = [113];
    this.map.setCollisionBetween(terrain_tileset_collision_array_from, terrain_tileset_collision_array_to, true, this.terrain_layer);

    this.terrain_layer.resizeWorld();
}

_state.createMapElements = function () {
    // mechanical door
    this.mechanical_door = [];
    this.globalConfig.mechanicalDoor.forEach(function(element) {
        var newMechanicalDoor = new MechanicalDoor(element.doorConfig, element.buttonConfig, element.interactionConfig);
        this.mechanical_door.push(newMechanicalDoor);
    }, this);

    // gateway
    this.gateway = [];
    this.globalConfig.gateway.forEach(function(element) {
        var newGateway = new GateWay(element.doorConfig, element.interactionConfig);
        this.gateway.push(newGateway);
    }, this);

    // floating platform test
    this.floating_platform = [];
    this.globalConfig.floatingPlatform.forEach(function(element) {
        var newFloatingPlatform = new FloatingPlatform(element.platformConfig, element.buttonConfig, element.moveConfig, element.interactionConfig);
        this.floating_platform.push(newFloatingPlatform);
    }, this);
    
    // trap
    this.trap = [];
    this.globalConfig.trap.forEach(function(element) {
        var newTrap = new Trap(element.trapConfig);
        this.trap.push(newTrap);
    }, this);

    // fountain
    this.fountain = [];
    this.globalConfig.fountain.forEach(function(element) {
        var newFountain = new Fountain(element.config);
        this.fountain.push(newFountain);
    }, this);

    // block
    this.blocks = game.add.group();
    this.globalConfig.blocks.forEach(function(element) {
        this.newBlock = new InteractionBlock(element.config, this.blocks);
    }, this);

    // Buff/Nerf Item
    this.item = game.add.group();
    this.globalConfig.BN_Item.forEach(function(element) {
        var newItem = new BN_Item(element.config, this.item);
    }, this);

    // torch
    this.globalConfig.Torch.forEach(function(element) {
        var newTorch = this.putTorch(element.x, element.y);
    }, this);

    this.goal = new Goal(this.globalConfig.Goal);
}

_state.putTorch = function (x, y) {
    var torch = game.add.sprite(x, y, 'torch');
    torch.anchor.setTo(0.5, 0.5);
    torch.animations.add('glow', [0, 1, 2], 8, true);
    torch.animations.play('glow');
}