_state.createCamera = function () {
    var cameraX = ( this.p1.x + this.p2.x ) / 2;
    var cameraY = ( this.p1.y + this.p2.y ) / 2;

    this.cameraObject = game.add.sprite(cameraX, cameraY, 'pixia');
    game.physics.arcade.enable(this.cameraObject);
    this.cameraObject.anchor.setTo(0.5, 1);
    this.cameraObject.alpha = 0;

    this.cameraObject.body.drag.x = 100;
    this.cameraObject.body.drag.y = 100;

    game.camera.follow(this.cameraObject);
    //game.camera.follow(this.cameraObject, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
}

_state.updateCamera = function () {
    //this.cameraObject.x = (this.p1.x + this.p2.x) / 2;
    //this.cameraObject.y = (this.p1.y + this.p2.y) / 2;

    var vectorX = (this.p1.x + this.p2.x) / 2 - this.cameraObject.x;
    var vectorY = (this.p1.y + this.p2.y) / 2 - this.cameraObject.y;

    if (Math.hypot(vectorX, vectorY) > 100) {
        this.cameraObject.body.velocity.x = vectorX;
        this.cameraObject.body.velocity.y = vectorY;   
    }
}

_state.createTextWindow = function() {
    //Textwindow
    this.textwindow = game.add.sprite(900, 550, 'TextWindow');
    this.textwindow.fixedToCamera = true;
    this.textwindow.anchor.x = 0.5;
    this.textwindow.alpha = 0;
    this.nextwindow = game.input.keyboard.addKey(Phaser.KeyCode.N);
    this.skip = game.input.keyboard.addKey(Phaser.KeyCode.S);
    this.tellstory = false;

    this.textcontent = game.add.text(300, 650, '嘻嘻', {font: 'bold 36pt 新細明體'});
    this.textcontent.fixedToCamera = true;
    this.textcontent.alpha = 0;
    this.line = 1;
}

_state.startStory = function(Story, pause) {
    if( Story.length != 0 ){
        this.tellstory = true;
        this.storyline(Story[0], pause);

        this.nextwindow.onDown.add(function(){
            if(this.tellstory){
                if(!Story[this.line]){
                    this.tellstory = false;
                    game.paused = false;
                    this.textwindow.alpha = 0;
                    this.textcontent.alpha = 0;
                    this.line = 1;
                }
                else {
                    this.storyline(Story[this.line], true);
                    this.line++;
                }
            }
        }, this);

        this.skip.onDown.add(function(){
            if(this.tellstory){
                this.tellstory = false;
                game.paused = false;
                this.textwindow.alpha = 0;
                this.textcontent.alpha = 0;
                this.line = 1;
            }
        }, this);
    }
}

_state.storyline = function(Story, pause) {
    if(this.tellstory){
        game.paused = true;
        this.textcontent.text = Story;
        this.textwindow.alpha = 1;
        this.textcontent.alpha = 1;
    }
}