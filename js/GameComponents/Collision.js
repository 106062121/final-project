_state.collisionHandler = function () {
    // map
    game.physics.arcade.collide(this.players, this.terrain_layer, this.PlayerVSMapCollideHandler, this.PlayerVSMapPreCollideHandler, this);
    game.physics.arcade.collide(this.blocks, this.terrain_layer, this.BlockVSMapCollideHandler, this.BlockVSMapPreCollideHandler, this);

    // blocks
    if (this.p1.y < this.p2.y) {
        game.physics.arcade.collide(this.p1, this.blocks, this.PlayerVSBlockCollideHandler, this.PlayerVSBlockPreCollideHandler, this);
        game.physics.arcade.collide(this.p2, this.blocks, this.PlayerVSBlockCollideHandler, this.PlayerVSBlockPreCollideHandler, this);
    }
    else {
        game.physics.arcade.collide(this.p2, this.blocks, this.PlayerVSBlockCollideHandler, this.PlayerVSBlockPreCollideHandler, this);
        game.physics.arcade.collide(this.p1, this.blocks, this.PlayerVSBlockCollideHandler, this.PlayerVSBlockPreCollideHandler, this);
    } 
    //game.physics.arcade.collide(this.players, this.blocks, this.PlayerVSBlockAfterCollideHandler, null, this);
    game.physics.arcade.collide(this.blocks);

    // player
    this.interacting = game.physics.arcade.overlap(this.p1, this.p2, this.PlayerVSPlayerCollideHandler, this.PlayerVSPlayerPreCollideHandler, this);
    
    // Item
    game.physics.arcade.overlap(this.players, this.item, this.PlayerVSItemCollideHandler, null, this);

    //Goal
    this.p1reach = game.physics.arcade.overlap(this.p1, this.goal);
    this.p2reach = game.physics.arcade.overlap(this.p2, this.goal);

    // mechanical door
    this.mechanical_door.forEach(function(element) {
        element.update(this.p1, this.p2, this.blocks, this.cameraObject);
    }, this);

    // gateway
    this.gateway.forEach(function(element) {
        element.update(this.p1, this.p2, this.blocks, this.cameraObject);
    }, this);

    // floating platform
    this.p1.onboard = false;
    this.p2.onboard = false;
    this.floating_platform.forEach(function(element) {
        element.update(this.p1, this.p2, this.blocks, this.cameraObject);
    }, this);

    // trap
    this.trap.forEach(function(element) {
        element.update(this.p1, this.p2, this.blocks, this.cameraObject);
    }, this);

    // fountain
    this.fountain.forEach(function (element) {
        element.update(this.p1, this.p2, this.blocks);
    }, this);
}

// collide callbacks
_state.PlayerVSMapCollideHandler = function (player, map) {
    if(player.flying){
        player.flying = false;
        player.body.gravity.y = game.global.gravity.normal;
    }
}

_state.BlockVSMapCollideHandler = function (block, map) {
    //console.log(block.body.deltaAbsY())
    if (block.body.onFloor() && block.body.deltaAbsY() > 1.5) {
        this.playFX('blockDrop');
    }
}

_state.PlayerVSBlockCollideHandler = function (player, block) {
    if (block.body.touching.left &&　block.body.touching.right && ((this.p1.body.touching.right && this.p2.body.touching.left) || (this.p2.body.touching.right && this.p1.body.touching.left))) {
        if ((this.p1Control.left.isDown && this.p2Control.right.isDown) || (this.p2Control.left.isDown && this.p1Control.right.isDown)) {
            block.body.velocity.y = -100;
            block.body.velocity.x = 0;
        }
    }

    if (block.body.touching.down) {
        if (this.p1.body.touching.up) {
            if (this.p1Control.left.isDown || this.p1Control.right.isDown) {
                block.body.velocity.x = this.p1.body.velocity.x;
            }
            else {
                block.body.velocity.x = 0;
            }
        }
        else if (this.p2.body.touching.up) {
            if (this.p2Control.left.isDown || this.p2Control.right.isDown) {
                block.body.velocity.x = this.p2.body.velocity.x;
            }
            else {
                block.body.velocity.x = 0;
            }
        }
    }

    if (block.body.onFloor() && (block.body.touching.left || block.body.touching.right) && !this.soundFX['blockMove'].isPlaying) {
        this.playFX('blockMove');
    }

    this.Blockfly(player, block);
}

_state.PlayerVSPlayerCollideHandler = function (p1, p2) {

}

// process callbacks
_state.PlayerVSMapPreCollideHandler = function (player, map) {

    return true;
}

_state.BlockVSMapPreCollideHandler = function (block, map) {

    return true;
}

_state.PlayerVSBlockPreCollideHandler = function (player, block) {
    if (block.y >= player.y + player.body.height) {
        player.body.velocity.y = block.body.velocity.y;
    }
    else if (block.y < player.y)  {
        block.body.velocity.y = player.body.velocity.y;
    }

    return true;
}

_state.PlayerVSPlayerPreCollideHandler = function (p1, p2) {

    return true;

}

_state.PlayerVSItemCollideHandler = function (player, item) {
    if(item.type == 'Z'){
        this.Zombinize(player, item);
    }
    else if(item.type == 'C'){
        this.Revive(player, item);
    }
}