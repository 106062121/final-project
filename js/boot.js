var bootState = {
    preload: function () {

    },
    create: function () {
        // Set some game settings.
        game.stage.backgroundColor = '#666675';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        //this.game.physics.arcade.gravity.y = 500;

        // Start the load state.
        game.state.start('load');
    }
}; 