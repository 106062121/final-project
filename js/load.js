var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        this.loadLabel = game.add.text(game.width/2, game.height*9/10, 'now loading...', { font: '30px Comic Sans MS', fill: '#ffffff' });
        this.loadLabel.anchor.setTo(0.5, 0.5);
        this.loadLabel.alpha = 0;

        game.add.tween(this.loadLabel).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

        // tile assets
        game.load.image('terrain_tileset', 'assets/normal/terrain_tileset.png');
        game.load.image('tilemap_v1.3', 'assets/normal/tilemap_v1.3.png');
        game.load.image('background_v1.2', 'assets/normal/background_v1.2.png');

        // background tilemap
        game.load.image('background', 'assets/normal/background.png');

        // background tilemap
        game.load.tilemap('mapBG', 'assets/normal/background.json', null, Phaser.Tilemap.TILED_JSON);

        // torch
        game.load.spritesheet('torch','assets/normal/torch.png', 240, 240);

        // camera object image
        game.load.image('pixel', 'assets/normal/pixel.png');

        // players image
        game.load.spritesheet('pixie','assets/normal/BlueCat.png', 80, 64);
        game.load.image('bigie', 'assets/normal/BlueCat_ZhaMao.png');

        game.load.spritesheet('pixia', 'assets/normal/RedCat.png', 80, 64);
        game.load.spritesheet('pixia2', 'assets/normal/RedCat_Interaction.png', 80, 128);
        game.load.image('bigia', 'assets/normal/RedCat_ZhaMao.png');

        // zombie player image
        game.load.spritesheet('enemy', 'assets/normal/BlackCat.png', 80, 64);

        // interaction arrow image
        game.load.image('shoot', 'assets/normal/arrow.png');

        // mechanism image
        // mechanical door image
        game.load.spritesheet('blue_mechanical_door_1_1', 'assets/normal/Mechanism_Object/MechanicalDoor/blue_door_1_1.png', 80, 80);
        game.load.spritesheet('blue_mechanical_door_1_2', 'assets/normal/Mechanism_Object/MechanicalDoor/blue_door_1_2.png', 80, 160);
        game.load.spritesheet('blue_mechanical_door_1_3', 'assets/normal/Mechanism_Object/MechanicalDoor/blue_door_1_3.png', 80, 240);
        game.load.spritesheet('blue_mechanical_door_1_4', 'assets/normal/Mechanism_Object/MechanicalDoor/blue_door_1_4.png', 80, 320);

        game.load.spritesheet('red_mechanical_door_1_1', 'assets/normal/Mechanism_Object/MechanicalDoor/red_door_1_1.png', 80, 80);
        game.load.spritesheet('red_mechanical_door_1_2', 'assets/normal/Mechanism_Object/MechanicalDoor/red_door_1_2.png', 80, 160);
        game.load.spritesheet('red_mechanical_door_1_3', 'assets/normal/Mechanism_Object/MechanicalDoor/red_door_1_3.png', 80, 240);
        game.load.spritesheet('red_mechanical_door_1_4', 'assets/normal/Mechanism_Object/MechanicalDoor/red_door_1_4.png', 80, 320);
        
        game.load.spritesheet('purple_mechanical_door_1_1', 'assets/normal/Mechanism_Object/MechanicalDoor/purple_door_1_1.png', 80, 80);
        game.load.spritesheet('purple_mechanical_door_1_2', 'assets/normal/Mechanism_Object/MechanicalDoor/purple_door_1_2.png', 80, 160);
        game.load.spritesheet('purple_mechanical_door_1_3', 'assets/normal/Mechanism_Object/MechanicalDoor/purple_door_1_3.png', 80, 240);
        game.load.spritesheet('purple_mechanical_door_1_4', 'assets/normal/Mechanism_Object/MechanicalDoor/purple_door_1_4.png', 80, 320);
        
        game.load.spritesheet('white_mechanical_door_1_1', 'assets/normal/Mechanism_Object/MechanicalDoor/white_door_1_1.png', 80, 80);
        game.load.spritesheet('white_mechanical_door_1_2', 'assets/normal/Mechanism_Object/MechanicalDoor/white_door_1_2.png', 80, 160);
        game.load.spritesheet('white_mechanical_door_1_3', 'assets/normal/Mechanism_Object/MechanicalDoor/white_door_1_3.png', 80, 240);
        game.load.spritesheet('white_mechanical_door_1_4', 'assets/normal/Mechanism_Object/MechanicalDoor/white_door_1_4.png', 80, 320);
        
        // mechanical door button image
        game.load.spritesheet('blue_mechanical_door_button', 'assets/normal/Mechanism_Object/MechanicalDoor/blue_button.png', 64, 20);
        game.load.spritesheet('red_mechanical_door_button', 'assets/normal/Mechanism_Object/MechanicalDoor/red_button.png', 64, 20);
        game.load.spritesheet('purple_mechanical_door_button', 'assets/normal/Mechanism_Object/MechanicalDoor/purple_button.png', 64, 20);
        game.load.spritesheet('white_mechanical_door_button', 'assets/normal/Mechanism_Object/MechanicalDoor/white_button.png', 64, 20);

        // gateway image
        game.load.spritesheet('blue_gateway', 'assets/normal/Mechanism_Object/GateWay/blue_gateway.png', 80, 120);
        game.load.spritesheet('red_gateway', 'assets/normal/Mechanism_Object/GateWay/red_gateway.png', 80, 120);
        game.load.spritesheet('purple_gateway', 'assets/normal/Mechanism_Object/GateWay/purple_gateway.png', 80, 120);
        game.load.spritesheet('white_gateway', 'assets/normal/Mechanism_Object/GateWay/white_gateway.png', 80, 120);

        /* should be w*h */
        // floating platform image
        game.load.spritesheet('blue_floating_platform_2_1', 'assets/normal/Mechanism_Object/FloatingPlatform/blue_platform_2_1.png', 160, 80);
        game.load.spritesheet('blue_floating_platform_3_1', 'assets/normal/Mechanism_Object/FloatingPlatform/blue_platform_3_1.png', 240, 80);
        game.load.spritesheet('blue_floating_platform_4_1', 'assets/normal/Mechanism_Object/FloatingPlatform/blue_platform_4_1.png', 320, 80);
        game.load.spritesheet('blue_floating_platform_5_1', 'assets/normal/Mechanism_Object/FloatingPlatform/blue_platform_5_1.png', 400, 80);
        
        game.load.spritesheet('red_floating_platform_2_1', 'assets/normal/Mechanism_Object/FloatingPlatform/red_platform_2_1.png', 160, 80);
        game.load.spritesheet('red_floating_platform_3_1', 'assets/normal/Mechanism_Object/FloatingPlatform/red_platform_3_1.png', 240, 80);
        game.load.spritesheet('red_floating_platform_4_1', 'assets/normal/Mechanism_Object/FloatingPlatform/red_platform_4_1.png', 320, 80);
        game.load.spritesheet('red_floating_platform_5_1', 'assets/normal/Mechanism_Object/FloatingPlatform/red_platform_5_1.png', 400, 80);
        
        game.load.spritesheet('purple_floating_platform_2_1', 'assets/normal/Mechanism_Object/FloatingPlatform/purple_platform_2_1.png', 160, 80);
        game.load.spritesheet('purple_floating_platform_3_1', 'assets/normal/Mechanism_Object/FloatingPlatform/purple_platform_3_1.png', 240, 80);
        game.load.spritesheet('purple_floating_platform_4_1', 'assets/normal/Mechanism_Object/FloatingPlatform/purple_platform_4_1.png', 320, 80);
        game.load.spritesheet('purple_floating_platform_5_1', 'assets/normal/Mechanism_Object/FloatingPlatform/purple_platform_5_1.png', 400, 80);
        
        game.load.spritesheet('white_floating_platform_2_1', 'assets/normal/Mechanism_Object/FloatingPlatform/white_platform_2_1.png', 160, 80);
        game.load.spritesheet('white_floating_platform_3_1', 'assets/normal/Mechanism_Object/FloatingPlatform/white_platform_3_1.png', 240, 80);
        game.load.spritesheet('white_floating_platform_4_1', 'assets/normal/Mechanism_Object/FloatingPlatform/white_platform_4_1.png', 320, 80);
        game.load.spritesheet('white_floating_platform_5_1', 'assets/normal/Mechanism_Object/FloatingPlatform/white_platform_5_1.png', 400, 80);        

        // floating platform button image
        game.load.spritesheet('blue_floating_platform_button', 'assets/normal/Mechanism_Object/FloatingPlatform/blue_button.png', 64, 20);
        game.load.spritesheet('red_floating_platform_button', 'assets/normal/Mechanism_Object/FloatingPlatform/red_button.png', 64, 20);
        game.load.spritesheet('purple_floating_platform_button', 'assets/normal/Mechanism_Object/FloatingPlatform/purple_button.png', 64, 20);
        game.load.spritesheet('white_floating_platform_button', 'assets/normal/Mechanism_Object/FloatingPlatform/white_button.png', 64, 20);

        // trap image
        game.load.image('trap_image', 'assets/normal/Mechanism_Object/Trap/sppike.png');

        // zombie image
        game.load.image('Posion', 'assets/normal/Mechanism_Object/BN_Item/posionbottle.png');
        game.load.image('Cure', 'assets/normal/Mechanism_Object/BN_Item/curebottle.png');

        // interaction block image
        game.load.image('Stone', 'assets/normal/Mechanism_Object/InteractionBlock/stone.png');
        game.load.image('WoodBox', 'assets/normal/Mechanism_Object/InteractionBlock/woodbox.png');

        // fountain image
        game.load.image('water', 'assets/normal/Mechanism_Object/Fountain/water.png');

        // goal image
        game.load.image('Goal', 'assets/normal/Mechanism_Object/Goal/Goal.png');

        // feather image
        game.load.image('red_feather', 'assets/normal/Feather/red_feather.png');
        game.load.image('red_feather_160', 'assets/normal/Feather/red_feather_160.png');
        
        game.load.image('yellow_feather', 'assets/normal/Feather/yellow_feather.png');
        game.load.image('yellow_feather_160', 'assets/normal/Feather/yellow_feather_160.png');

        game.load.image('green_feather', 'assets/normal/Feather/green_feather.png');
        game.load.image('green_feather_160', 'assets/normal/Feather/green_feather_160.png');

        game.load.image('blue_feather', 'assets/normal/Feather/blue_feather.png');
        game.load.image('blue_feather_160', 'assets/normal/Feather/blue_feather_160.png');

        game.load.image('pink_feather', 'assets/normal/Feather/pink_feather.png');
        game.load.image('pink_feather_160', 'assets/normal/Feather/pink_feather_160.png');        

        // teaching image
        game.load.image('P1ctrl','assets/normal/Teaching_Level/p1move.png' );
        game.load.image('P2ctrl','assets/normal/Teaching_Level/p2move.png' );
        game.load.image('Mech', 'assets/normal/Teaching_Level/Mech.png');
        game.load.image('MechBox', 'assets/normal/Teaching_Level/MechBox.png');
        game.load.image('Shoot', 'assets/normal/Teaching_Level/Shoot.png');
        game.load.image('Plat', 'assets/normal/Teaching_Level/Plat.png');
        game.load.image('Boom', 'assets/normal/Teaching_Level/Boom.png');
        game.load.image('Gate', 'assets/normal/Teaching_Level/Gate.png');
        game.load.image('Fount', 'assets/normal/Teaching_Level/Fount.png');
        game.load.image('Fountbox', 'assets/normal/Teaching_Level/Fountbox.png');
        game.load.image('Dead', 'assets/normal/Teaching_Level/Dead.png');

        // storyline
        game.load.image('TextWindow', 'assets/normal/TextWindow.png');

        // sound fx
        game.load.audio('blockDrop', 'assets/audio/_blockDrop.mp3');
        game.load.audio('blockMove', 'assets/audio/_blockMove.wav');
        game.load.audio('BN_cure', 'assets/audio/_BN_cure.mp3');
        game.load.audio('BN_zombie', 'assets/audio/_BN_zombie.mp3');
        game.load.audio('buttonClick', 'assets/audio/_buttonClick.mp3');
        game.load.audio('floatingPlatformMove', 'assets/audio/_floatingPlatformMove.mp3');
        game.load.audio('floatingPlatformTrigger', 'assets/audio/_floatingPlatformTrigger.mp3');
        game.load.audio('fountain', 'assets/audio/_fountain.mp3'); //
        game.load.audio('gateway', 'assets/audio/_gateway.mp3');
        game.load.audio('mechanicalDoorMove', 'assets/audio/_mechanicalDoorMove.mp3');
        game.load.audio('mechanicalDoorTrigger', 'assets/audio/_mechanicalDoorTrigger.mp3');
        game.load.audio('trap', 'assets/audio/_trap.mp3');
        game.load.audio('ZaMao', 'assets/audio/_ZaMao.wav');
        game.load.audio('win', 'assets/audio/_win.wav');
        game.load.audio('fail', 'assets/audio/_fail.wav');

        //bgm
        game.load.audio('bgm01', 'assets/audio/_BGM ICE - Entrance Full Song.mp3');
        game.load.audio('bgm02', 'assets/audio/_BGM NieR Automata OST - Amusement Park (8-bit).mp3');
        game.load.audio('bgm03', 'assets/audio/_BGM NieR Automata OST - City Ruins - Rays of Light (8-bit).mp3');
        game.load.audio('bgm04', 'assets/audio/_BGM NieR Automata OST - Wretched Weaponry (8-bit).mp3');
    },
    create: function() {
        this.loadLabel.kill();
        // fx
        buttonClick = game.add.audio('buttonClick');

        game.add.image(0, 0, 'background');

        this.titleLabel = game.add.text(game.width/2, 0, 'Legend Feathers', { font: '70px Comic Sans MS', fill: '#ffffff' });
        this.titleLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(this.titleLabel).to( { y: game.height/4 }).start();

        // Go to the menu state
        this.loadingLabel = game.add.text(game.width/2, game.height*9/10, 'now loading...', { font: '30px Comic Sans MS', fill: '#ffffff' });
        this.loadingLabel.anchor.setTo(0.5, 0.5);
        this.loadingLabel.alpha = 0;

        game.add.tween(this.loadingLabel).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        this.loadingLabel.setText('click to start');
        this.loadingLabel.anchor.setTo(0.5, 0.5);
        this.loadingLabel.inputEnabled = true;
        this.loadingLabel.events.onInputOver.add(function () {
            this.loadingLabel.setStyle({ font: '30px Comic Sans MS', fill: '#000000' });
        }, this);
        this.loadingLabel.events.onInputOut.add(function () {
            this.loadingLabel.setStyle({ font: '30px Comic Sans MS', fill: '#ffffff' });
        }, this);
        this.loadingLabel.events.onInputDown.add(this.start, this);

        //game.state.start('debug', true, false, Level_LIU);
    },
    start: function () {
        this.createTextWindow();
        buttonClick.play();
        this.loadingLabel.kill();
        this.startStory(Gamebegin);
        game.state.start('menu');
    }
}; 

loadState.createTextWindow = function() {
    //Textwindow
    this.textwindow = game.add.sprite(900, 550, 'TextWindow');
    this.textwindow.fixedToCamera = true;
    this.textwindow.anchor.x = 0.5;
    this.textwindow.alpha = 0;
    this.nextwindow = game.input.keyboard.addKey(Phaser.KeyCode.N);
    this.skip = game.input.keyboard.addKey(Phaser.KeyCode.S);
    this.tellstory = false;

    this.textcontent = game.add.text(300, 650, '嘻嘻', {font: 'bold 36pt 新細明體'});
    this.textcontent.fixedToCamera = true;
    this.textcontent.alpha = 0;
    this.line = 1;
}

loadState.startStory = function(Story, pause) {
    if( Story.length != 0 ){
        this.tellstory = true;
        this.storyline(Story[0], pause);

        this.nextwindow.onDown.add(function(){
            if(this.tellstory){
                if(!Story[this.line]){
                    this.tellstory = false;
                    game.paused = false;
                    this.textwindow.alpha = 0;
                    this.textcontent.alpha = 0;
                    this.line = 1;
                }
                else {
                    this.storyline(Story[this.line], true);
                    this.line++;
                }
            }
        }, this);

        this.skip.onDown.add(function(){
            if(this.tellstory){
                this.tellstory = false;
                game.paused = false;
                this.textwindow.alpha = 0;
                this.textcontent.alpha = 0;
                this.line = 1;
            }
        }, this);
    }
}

loadState.storyline = function(Story, pause) {
    if(this.tellstory){
        game.paused = true;
        this.textcontent.text = Story;
        this.textwindow.alpha = 1;
        this.textcontent.alpha = 1;
    }
}