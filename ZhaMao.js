// maybe i can create a invisible emitter to collide ?
debugScene.ZhaMao = function () {
    game.physics.arcade.collide(this.p1, this.terrain_layer, this.TileMapZhaMaoCollideHandler, null, this);
    game.physics.arcade.collide(this.p1, this.mechanism_layer, this.TileMapZhaMaoCollideHandler, null, this);
    game.physics.arcade.collide(this.p1, this.blocks, this.SpriteZhaMaoCollideHandler, null, this); 

    game.physics.arcade.collide(this.p2, this.terrain_layer, this.TileMapZhaMaoCollideHandler, null, this);
    game.physics.arcade.collide(this.p2, this.mechanism_layer, this.TileMapZhaMaoCollideHandler, null, this);
    game.physics.arcade.collide(this.p2, this.blocks, this.SpriteZhaMaoCollideHandler, null, this);

    game.physics.arcade.overlap(this.p1, this.p2, this.PlayerZhaMaoCollideHandler, null, this);

}

debugScene.PlayerZhaMaoCollideHandler = function (sprite1, sprite2) {
    var dx = sprite1.x - sprite2.x > 0 ? 1 : -1;
    var dy = sprite1.y - sprite2.y > 0 ? 1 : -1;
    sprite1.body.velocity.x = dx * 250;
    sprite1.body.velocity.y = dy * 250;

    sprite2.body.velocity.x = -dx * 250;
    sprite2.body.velocity.y = -dy * 250;
}

debugScene.SpriteZhaMaoCollideHandler = function (sprite1, sprite2) {
    var dx = sprite1.x - sprite2.x > 0 ? 1 : -1;
    var dy = sprite1.y - sprite2.y > 0 ? 1 : -1;
    sprite1.body.velocity.x = dx * 250;
    sprite1.body.velocity.y = dy * 250;

    sprite2.body.velocity.x = -dx * 250;
    sprite2.body.velocity.y = -dy * 250;
}

debugScene.TileMapZhaMaoCollideHandler = function (sprite, tile) {
    if(this.interactKey.isDown) {
        var dx = sprite.x - (tile.x * tile.width - tile.width / 2) > 0 ? 1 : -1;
        var dy = sprite.y - (tile.y * tile.width) > 0 ? 1 : -1;
        sprite.body.velocity.x = dx * 250;
        sprite.body.velocity.y = dy * 250;
    }
}